# EasyEL

#### 介绍
一个简单高效的表达式引擎


#### 安装教程

```xml
    <!-- 暂未发布中央仓库, 敬请期待 -->
    <dependency>
        <groupId>com.dtflyx</groupId>
        <artifactId>easy-el</artifactId>
        <version>1.0.0</version>
    </dependency>
```

#### Hello World

```java
EasyElContext context = new EasyElContext();
Object ret = EasyEl.eval("'hello world!'", context);
System.out.println(ret);
```


#### 详细文档

[新手指导](src/docs/guide.md)
