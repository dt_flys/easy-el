package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTList extends ASTExpression {

    private final List<ASTExpression> listItems;

    public ASTList(List<ASTExpression> listItems) {
        super(ASTType.LIST);
        this.listItems = listItems;
    }

    public List<ASTExpression> getListItems() {
        return listItems;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (int i = 0; i < listItems.size(); i++) {
            ASTExpression item = listItems.get(i);
            builder.append(item);
            if (i < listItems.size() - 1) {
                builder.append(", ");
            }
        }
        builder.append(']');
        return builder.toString();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitList(context, this);
        return null;
    }
}
