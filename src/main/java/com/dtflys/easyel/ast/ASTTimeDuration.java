package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTTimeDuration extends ASTTime {

    public ASTTimeDuration() {
        super(ASTType.TIME_DURATION);
    }


    @Override
    public String getText() {
        return getTimePartText();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitTimeDuration(context, this);
        return null;
    }
}
