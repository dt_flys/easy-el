package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTListGenerator extends ASTExpression {

    private final ASTExpression item;

    private final List<ASTIdentifier> nameList;

    private final ASTExpression forCondition;

    public ASTListGenerator(ASTExpression item, List<ASTIdentifier> nameList, ASTExpression forCondition) {
        super(ASTType.LIST);
        this.item = item;
        this.nameList = nameList;
        this.forCondition = forCondition;
    }

    public ASTExpression getItem() {
        return item;
    }

    public List<ASTIdentifier> getNameList() {
        return nameList;
    }

    public ASTExpression getForCondition() {
        return forCondition;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        builder.append(item.toString());
        builder.append(" for ");
        for (int i = 0; i < nameList.size(); i++) {
            ASTExpression name = nameList.get(i);
            builder.append(name.getText());
            if (i < nameList.size() - 1) {
                builder.append(", ");
            }
        }
        builder.append(" in ");
        builder.append(forCondition.toString());
        builder.append(']');
        return builder.toString();
    }

    @Override
    public String toString() {
        return this.getText();
    }


    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitListGenerator(context, this);
        return null;
    }
}
