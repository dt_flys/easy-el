package com.dtflys.easyel.ast;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class ASTExpression extends ASTNode {

    private ASTType type;

    public ASTExpression(ASTType type) {
        this.type = type;
    }

    public ASTType getType() {
        return type;
    }

    public abstract String getText();
}
