package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTIdentifier extends ASTExpression {

    private final String name;


    public ASTIdentifier(String name) {
        super(ASTType.UNDEFINED);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getText() {
        return name;
    }

    @Override
    public String toString() {
        return "<" + name + ">";
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitIdentifier(context, this);
        return null;
    }
}
