package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public interface ASTVisitor {

    void visitType(EasyElRuntimeContext context, ASTType node);

    void visitConstant(EasyElRuntimeContext context, ASTConstant node);

    void visitRange(EasyElRuntimeContext context, ASTRange node);

    void visitDate(EasyElRuntimeContext context, ASTDate node);

    void visitTimeDuration(EasyElRuntimeContext context, ASTTimeDuration node);

    void visitList(EasyElRuntimeContext context, ASTList node);

    void visitListGenerator(EasyElRuntimeContext context, ASTListGenerator node);

    void visitMap(EasyElRuntimeContext context, ASTMap node);

    void visitMapEntry(EasyElRuntimeContext context, ASTMapEntry node);

    void visitNull(EasyElRuntimeContext context, ASTNull node);

    void visitNewInstance(EasyElRuntimeContext context, ASTNewInstance node);

    void visitIdentifier(EasyElRuntimeContext context, ASTIdentifier node);

    void visitBinaryExpression(EasyElRuntimeContext context, ASTBinaryExpression node);

    void visitTernaryExpression(EasyElRuntimeContext context, ASTTernaryExpression node);

    void visitNegativeExpression(EasyElRuntimeContext context, ASTNegativeExpression node);

    void visitNotExpression(EasyElRuntimeContext context, ASTNotExpression node);

    void visitAccessExpression(EasyElRuntimeContext context, ASTAccessExpression node);

    void visitIndexExpression(EasyElRuntimeContext context, ASTIndexExpression node);

    void visitInvokeExpression(EasyElRuntimeContext context, ASTInvokeExpression node);
}
