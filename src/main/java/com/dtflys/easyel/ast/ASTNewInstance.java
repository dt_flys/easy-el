package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTNewInstance extends ASTArguments {

    private ASTMap properties;

    public ASTNewInstance(ASTType type, List<ASTExpression> arguments) {
        super(type, arguments);
    }

    public ASTMap getProperties() {
        return properties;
    }

    public void setProperties(ASTMap properties) {
        this.properties = properties;
    }

    @Override
    public String getText() {
        return "new " + getType().getClazz().getName() + super.getText();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitNewInstance(context, this);
        return null;
    }
}
