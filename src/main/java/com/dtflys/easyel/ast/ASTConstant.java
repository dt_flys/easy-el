package com.dtflys.easyel.ast;

import com.dtflys.easyel.parser.EToken;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTConstant extends ASTExpression {
    
    private final EToken token;

    private final Object value;

    public ASTConstant(Object value, ASTType type) {
        this(value, type, null);
    }

    public ASTConstant(Object value, ASTType type, EToken token) {
        super(type);
        this.value = value;
        this.token = token;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public String getText() {
        return value.toString();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitConstant(context, this);
        return null;
    }
}
