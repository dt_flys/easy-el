package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTBinaryExpression extends ASTExpression {

    private final ASTExpression left;
    private final ASTExpression right;
    private final Token operation;


    public ASTBinaryExpression(ASTType type, ASTExpression left, Token operation, ASTExpression right) {
        super(type);
        this.left = left;
        this.right = right;
        this.operation = operation;
    }

    public ASTExpression getLeft() {
        return left;
    }


    public ASTExpression getRight() {
        return right;
    }


    public Token getOperation() {
        return operation;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        builder.append(left);
        builder.append(' ');
        builder.append(operation.getText());
        builder.append(' ');
        builder.append(right);
        builder.append(')');
        return builder.toString();
    }

    @Override
    public String getText() {
        return toString();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitBinaryExpression(context, this);
        return null;
    }
}
