package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTRange extends ASTExpression {

    private final ASTExpression from;
    private final ASTExpression to;
    private final boolean includeFrom;
    private final boolean includeTo;


    public ASTRange(ASTExpression from, ASTExpression to, boolean includeFrom, boolean includeTo) {
        super(ASTType.RANGE);
        this.from = from;
        this.to = to;
        this.includeFrom = includeFrom;
        this.includeTo = includeTo;
    }

    public ASTExpression getFrom() {
        return from;
    }

    public ASTExpression getTo() {
        return to;
    }

    public boolean isIncludeFrom() {
        return includeFrom;
    }

    public boolean isIncludeTo() {
        return includeTo;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append(includeFrom ? '[' : '(');
        if (from != null) {
            builder.append(from);
        }
        builder.append(" .. ");
        if (to != null) {
            builder.append(to);
        }
        builder.append(includeTo ? ']' : ')');
        return builder.toString();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitRange(context, this);
        return null;
    }
}
