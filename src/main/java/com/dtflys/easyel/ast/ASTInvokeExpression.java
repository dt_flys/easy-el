package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTInvokeExpression extends ASTArguments {

    private ASTExpression left;

    private ASTConstant methodName;

    private boolean isStatic = false;

    private ASTType staticType;


    public ASTInvokeExpression(List<ASTExpression> arguments) {
        super(ASTType.UNDEFINED, arguments);
    }

    public ASTExpression getLeft() {
        return left;
    }

    public void setLeft(ASTExpression left) {
        this.left = left;
    }

    public ASTConstant getMethodName() {
        return methodName;
    }

    public void setMethodName(ASTConstant methodName) {
        this.methodName = methodName;
    }

    public List<ASTExpression> getArguments() {
        return arguments;
    }


    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        if (left != null) {
            builder.append(left.toString());
            builder.append('.');
        } else if (isStatic && staticType != null) {
            builder.append(staticType.toString());
            builder.append('.');
        }
        builder.append(methodName);
        builder.append(super.getText());
        return builder.toString();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitInvokeExpression(context, this);
        return null;
    }

    @Override
    public String toString() {
        return getText();
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public ASTType getStaticType() {
        return staticType;
    }

    public void setStaticType(ASTType staticType) {
        this.staticType = staticType;
    }
}
