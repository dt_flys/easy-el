package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTAccessExpression extends ASTExpression {

    private ASTExpression left;
    private final ASTConstant accessName;
    private boolean isStatic = false;

    public ASTAccessExpression(ASTConstant accessName) {
        super(ASTType.UNDEFINED);
        this.accessName = accessName;
    }

    @Override
    public String getText() {
        return left.toString() + '.' + accessName.getText();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitAccessExpression(context, this);
        return null;
    }

    public ASTExpression getLeft() {
        return left;
    }

    public void setLeft(ASTExpression left) {
        this.left = left;
    }

    public ASTConstant getAccessName() {
        return accessName;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }
}
