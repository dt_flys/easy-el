package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTMap extends ASTExpression {

    private final List<ASTMapEntry> entryList;

    public ASTMap(List<ASTMapEntry> entryList) {
        super(ASTType.MAP);
        this.entryList = entryList;
    }

    public List<ASTMapEntry> getEntryList() {
        return entryList;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append('{');
        for (int i = 0; i < entryList.size(); i++) {
            ASTMapEntry entry = entryList.get(i);
            builder.append(entry.getText());
            if (i < entryList.size() - 1) {
                builder.append(", ");
            }
        }
        builder.append('}');
        return builder.toString();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitMap(context, this);
        return null;
    }
}
