package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

public class ASTUnaryExpression extends ASTExpression {
    
    private final ASTExpression operation;

    private final ASTExpression expression;
    
    public ASTUnaryExpression(ASTType type, ASTExpression operation, ASTExpression expression) {
        super(type);
        this.operation = operation;
        this.expression = expression;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        builder.append(operation.getText());
        builder.append(' ');
        builder.append(expression);
        builder.append(')');
        return builder.toString();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        return "";
    }
}
