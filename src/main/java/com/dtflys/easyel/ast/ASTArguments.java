package com.dtflys.easyel.ast;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class ASTArguments extends ASTExpression {

    protected final List<ASTExpression> arguments;

    public ASTArguments(ASTType type, List<ASTExpression> arguments) {
        super(type);
        this.arguments = arguments;
    }

    protected String getArgumentsText() {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        for (int i = 0; i < arguments.size(); i++) {
            ASTExpression argExpr = arguments.get(i);
            builder.append(argExpr.toString());
            if (i < arguments.size() - 1) {
                builder.append(", ");
            }
        }
        builder.append(')');
        return builder.toString();
    }


    @Override
    public String getText() {
        return getArgumentsText();
    }


    public List<ASTExpression> getArguments() {
        return arguments;
    }
}
