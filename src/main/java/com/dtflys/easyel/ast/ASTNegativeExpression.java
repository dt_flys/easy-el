package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTNegativeExpression extends ASTExpression {

    private final ASTExpression expression;


    public ASTNegativeExpression(ASTType type, ASTExpression expression) {
        super(type);
        this.expression = expression;
    }

    public ASTExpression getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String getText() {
        return "-" + expression;
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitNegativeExpression(context, this);
        return null;
    }
}
