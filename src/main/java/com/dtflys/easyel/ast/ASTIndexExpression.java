package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTIndexExpression extends ASTExpression {

    private ASTExpression left;
    private final ASTExpression index;

    public ASTIndexExpression(ASTExpression index) {
        super(ASTType.UNDEFINED);
        this.index = index;
    }

    public ASTExpression getLeft() {
        return left;
    }

    public void setLeft(ASTExpression left) {
        this.left = left;
    }

    public ASTExpression getIndex() {
        return index;
    }

    @Override
    public String getText() {
        StringBuilder builder = new StringBuilder();
        builder.append(left);
        builder.append('[');
        builder.append(index);
        builder.append(']');
        return builder.toString();
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitIndexExpression(context, this);
        return null;
    }
}
