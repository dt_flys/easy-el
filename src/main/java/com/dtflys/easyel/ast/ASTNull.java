package com.dtflys.easyel.ast;


import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTNull extends ASTConstant {
    public ASTNull() {
        super(null, ASTType.OBJECT);
    }

    @Override
    public String toString() {
        return "<null>";
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitNull(context, this);
        return null;
    }
}
