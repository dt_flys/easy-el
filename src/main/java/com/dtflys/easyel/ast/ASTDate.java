package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTDate extends ASTTime {

    private ASTExpression year;
    private ASTExpression month;
    private ASTExpression day;
//    private ASTExpression milliseconds;

    public ASTDate() {
        super(ASTType.DATE);
    }

    public ASTExpression getYear() {
        return year;
    }

    public void setYear(ASTExpression year) {
        this.year = year;
    }

    public ASTExpression getMonth() {
        return month;
    }

    public void setMonth(ASTExpression month) {
        this.month = month;
    }

    public ASTExpression getDay() {
        return day;
    }

    public void setDay(ASTExpression day) {
        this.day = day;
    }


    @Override
    public String getText() {
        String date = year + "-" +formatDNumber(month.getText()) + "-"
                + formatDNumber(day.getText())
                + (hours != null || minutes != null || seconds != null ? " " : "")
                + getTimePartText();
        return date;
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitDate(context, this);
        return null;
    }
}
