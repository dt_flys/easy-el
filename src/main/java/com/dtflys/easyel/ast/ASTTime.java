package com.dtflys.easyel.ast;


/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class ASTTime extends ASTExpression {

    protected ASTExpression hours;
    protected ASTExpression minutes;
    protected ASTExpression seconds;
    protected ASTExpression milliseconds;


    public ASTTime(ASTType type) {
        super(type);
    }

    protected String formatDNumber(String num) {
        if (num.length() == 1) {
            return "0" + num;
        }
        return num;
    }

    protected String getTimePartText() {
        String time = "";
        if (hours != null && minutes != null) {
            time += formatDNumber(hours.getText()) + ":" + formatDNumber(minutes.getText());
            if (seconds != null) {
                time += ":" + formatDNumber(seconds.getText());
                if (milliseconds != null) {
                    time += ":" + formatDNumber(milliseconds.getText());
                }
            }
        }
        return time;
    }


    public ASTExpression getHours() {
        return hours;
    }

    public void setHours(ASTExpression hours) {
        this.hours = hours;
    }

    public ASTExpression getMinutes() {
        return minutes;
    }

    public void setMinutes(ASTExpression minutes) {
        this.minutes = minutes;
    }

    public ASTExpression getSeconds() {
        return seconds;
    }

    public void setSeconds(ASTExpression seconds) {
        this.seconds = seconds;
    }

    public ASTExpression getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(ASTExpression milliseconds) {
        this.milliseconds = milliseconds;
    }
}
