package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTNotExpression extends ASTExpression {

    private final ASTExpression expression;

    public ASTNotExpression(ASTExpression expression) {
        super(ASTType.BOOLEAN);
        this.expression = expression;
    }

    public ASTExpression getExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return getText();
    }

    @Override
    public String getText() {
        return "not " + expression;
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitNotExpression(context, this);
        return null;
    }
}
