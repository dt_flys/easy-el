package com.dtflys.easyel.ast;

import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ASTMapEntry extends ASTExpression {

    private final ASTExpression key;
    private final ASTExpression value;


    public ASTMapEntry(ASTExpression key, ASTExpression value) {
        super(ASTType.MAP_ENTRY);
        this.key = key;
        this.value = value;
    }

    public ASTExpression getKey() {
        return key;
    }

    public ASTExpression getValue() {
        return value;
    }

    @Override
    public String getText() {
        return key.getText() + ": " + value;
    }

    @Override
    public String visit(EasyElRuntimeContext context, ASTVisitor visitor) {
        visitor.visitMapEntry(context, this);
        return null;
    }
}
