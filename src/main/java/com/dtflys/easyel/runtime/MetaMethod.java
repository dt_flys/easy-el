package com.dtflys.easyel.runtime;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class MetaMethod {

    private final String methodName;

    private MethodParameterTypes paramTypes;



    public MetaMethod(String methodName, Class[] parameterTypes, boolean isVarArgs) {
        this.methodName = methodName;
        this.paramTypes = new MethodParameterTypes(parameterTypes, isVarArgs);
    }

    public String getMethodName() {
        return methodName;
    }

    public MethodParameterTypes getParamTypes() {
        return paramTypes;
    }

//    public Object invokeWithEmptyArguments(Object self) throws InvocationTargetException, IllegalAccessException {
//        return invoke(self, EMPTY_ARGUMENTS);
//    }

//    public abstract Object invoke(Object self, Object[] arguments) throws InvocationTargetException, IllegalAccessException;

    public String getMethodDeclarationName() {
        StringBuilder builder = new StringBuilder();
        builder.append(methodName);
        builder.append(" (");
        Class[] pTypes = this.paramTypes.getTypes();
        for (int i = 0; i < pTypes.length; i++) {
            Class pt = pTypes[i];
            builder.append(pt.getSimpleName());
            if (i < pTypes.length - 1) {
                builder.append(", ");
            }
        }
        builder.append(")");
        return builder.toString();
    }
}
