package com.dtflys.easyel.runtime;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MetaConstructor extends MetaMethod {


    private final Constructor constructor;

    public MetaConstructor(Constructor constructor) {
        super("<INIT>", constructor.getParameterTypes(), constructor.isVarArgs());
        this.constructor = constructor;
    }


    public Object newInstance(Object[] arguments) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        return constructor.newInstance(arguments);
    }

}
