package com.dtflys.easyel.runtime;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MetaProperty {

    private final String name;

    private Method getterMethod;

    private Method setterMethod;

    public MetaProperty(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Method getGetterMethod() {
        return getterMethod;
    }

    public void setGetterMethod(Method getterMethod) {
        this.getterMethod = getterMethod;
    }

    public Method getSetterMethod() {
        return setterMethod;
    }

    public void setSetterMethod(Method setterMethod) {
        this.setterMethod = setterMethod;
    }

    public boolean isReadable() {
        return getterMethod != null;
    }

    public boolean isWritable() {
        return setterMethod != null;
    }

    public Object getValue(Object obj) throws InvocationTargetException, IllegalAccessException {
        return getterMethod.invoke(obj);
    }

    public void setValue(Object obj, Object value) throws InvocationTargetException, IllegalAccessException {
        setterMethod.invoke(obj, new Object[] {value});
    }

}
