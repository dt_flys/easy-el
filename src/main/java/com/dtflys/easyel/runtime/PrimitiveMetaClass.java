package com.dtflys.easyel.runtime;

import com.dtflys.easyel.exception.EasyElAccessException;
import com.dtflys.easyel.exception.EasyElAmbiguousMethodsException;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class PrimitiveMetaClass extends MetaClass {


    public PrimitiveMetaClass(Class<?> clazz, int[] primrayTypeDistanceTable) {
        super(clazz);
        setTypeDistance(byte.class, primrayTypeDistanceTable[0]);
        setTypeDistance(Byte.class, primrayTypeDistanceTable[1]);
        setTypeDistance(short.class, primrayTypeDistanceTable[2]);
        setTypeDistance(Short.class, primrayTypeDistanceTable[3]);
        setTypeDistance(int.class, primrayTypeDistanceTable[4]);
        setTypeDistance(Integer.class, primrayTypeDistanceTable[5]);
        setTypeDistance(long.class, primrayTypeDistanceTable[6]);
        setTypeDistance(Long.class, primrayTypeDistanceTable[7]);
        setTypeDistance(BigInteger.class, primrayTypeDistanceTable[8]);
        setTypeDistance(float.class, primrayTypeDistanceTable[9]);
        setTypeDistance(Float.class, primrayTypeDistanceTable[10]);
        setTypeDistance(double.class, primrayTypeDistanceTable[11]);
        setTypeDistance(Double.class, primrayTypeDistanceTable[12]);
        setTypeDistance(BigDecimal.class, primrayTypeDistanceTable[13]);
        setTypeDistance(Number.class, primrayTypeDistanceTable[14]);
        setTypeDistance(Object.class, primrayTypeDistanceTable[15]);
    }

    @Override
    public Object access(Object self, String name) throws InvocationTargetException, IllegalAccessException, EasyElAccessException {
        if ("metaClass".equals(name)) {
            return this;
        }
        if ("getMetaClass".equals(name)) {
            return this;
        }
        return super.access(self, name);
    }

    @Override
    public Object invokeMethod(Object self, String methodName, Object[] arguments) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException {
        if ("getMetaClass".equals(methodName) && arguments.length == 0) {
            return this;
        }
        return super.invokeMethod(self, methodName, arguments);
    }
}
