package com.dtflys.easyel.runtime;

import com.dtflys.easyel.exception.EasyElAccessException;
import com.dtflys.easyel.exception.EasyElAmbiguousMethodsException;

import java.lang.reflect.InvocationTargetException;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElObjectSupport implements EasyElObject {

    private final MetaClass metaClass;

    public EasyElObjectSupport() {
        this.metaClass = MetaClassFactory.getMetaClass(this.getClass());
    }

    @Override
    public Object access(String name) throws IllegalAccessException, EasyElAccessException, InvocationTargetException {
        return metaClass.access(this, name);
    }

    @Override
    public void inject(String name, Object value) throws IllegalAccessException, EasyElAccessException, InvocationTargetException {
        metaClass.inject(this, name, value);
    }

    @Override
    public Object invokeMethod(String methodName, Object[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException {
        return metaClass.invokeMethod(this, methodName, args);
    }

    @Override
    public MetaClass getMetaClass() {
        return metaClass;
    }
}
