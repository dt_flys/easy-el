package com.dtflys.easyel.runtime;

import com.dtflys.easyel.exception.EasyElAccessException;
import com.dtflys.easyel.exception.EasyElAmbiguousMethodsException;

import java.lang.reflect.InvocationTargetException;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class JavaBeanMetaClass extends MetaClass {

    public JavaBeanMetaClass(Class<?> clazz) {
        super(clazz);
    }

    @Override
    public Object access(Object self, String name) throws InvocationTargetException, IllegalAccessException, EasyElAccessException {
        if ("metaClass".equals(name)) {
            return this;
        }
        if ("getMetaClass".equals(name)) {
            return this;
        }
        return super.access(self, name);
    }

    @Override
    public Object invokeMethod(Object self, String methodName, Object[] arguments) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException {
        if ("getMetaClass".equals(methodName) && arguments.length == 0) {
            return this;
        }
        return super.invokeMethod(self, methodName, arguments);
    }
}
