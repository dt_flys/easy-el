package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.utils.MetaHelper;
import org.antlr.v4.runtime.Token;

import java.util.Collection;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class InEvaluator {

    public Boolean evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {
        left = MetaHelper.unwrap(left);
        right = MetaHelper.unwrap(right);

        Class rightClass = right.getClass();
        Token op = node.getOperation();

        if (right instanceof String) {
            return ((String) right).contains(left.toString());
        }

        if (!(right instanceof Collection)) {
            throw new EasyElEvalException(context, node,
                    "operator " + op.getText() + " do not support for type " + rightClass);
        }
        Collection rightCollection = (Collection) right;
        return rightCollection.contains(left);
    }

}
