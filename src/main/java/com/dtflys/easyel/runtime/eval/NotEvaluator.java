package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTNotExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * 求值器
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class NotEvaluator {

    public Boolean evaluate(EasyElRuntimeContext context, ASTNotExpression node, Object obj) {
        obj = MetaHelper.unwrap(obj);
        Class clazz = obj.getClass();
        if (!(obj instanceof Boolean)) {
            throw new EasyElEvalException(context, node,
                    "operator ('not' | '!') do not support for type " + clazz);
        }
        Boolean boolValue = (Boolean) obj;
        return !boolValue;
    }

}
