package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.runtime.wrapper.NumberWrapper;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class DivideEvaluator {

    public Object evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {
        left = MetaHelper.wrap(left);
        right = MetaHelper.wrap(right);

        if (left instanceof NumberWrapper && right instanceof NumberWrapper) {
            return ((NumberWrapper) left).div((NumberWrapper) right);
        }

/*
        Class leftClass = left.getClass();
        Class rightClass = right.getClass();
        if (TypeHelper.isInteger(leftClass) || TypeHelper.isNumeric(leftClass)) {
            BigDecimal leftNum;
            if (left instanceof BigDecimal) {
                leftNum = (BigDecimal) left;
            } else {
                leftNum = new BigDecimal(left + "");
            }
            if (TypeHelper.isInteger(rightClass) || TypeHelper.isNumeric(rightClass)) {
                BigDecimal rightNum;
                if (right instanceof BigDecimal) {
                    rightNum = (BigDecimal) right;
                } else {
                    rightNum = new BigDecimal(right + "");
                }
                return leftNum.divide(rightNum);
            }
        }

        if (TypeHelper.isTimeDuration(leftClass)) {
            EasyElTimeDuration leftDuration = (EasyElTimeDuration) left;
            if (TypeHelper.isInteger(rightClass)) {
                BigDecimal rightNum;
                if (right instanceof BigDecimal) {
                    rightNum = (BigDecimal) right;
                } else {
                    rightNum = new BigDecimal(right + "");
                }
                return leftDuration.divide(rightNum.intValue());
            }
        }
*/

        throw new EasyElEvalException(context, node,
                "operator '/' do not support for type " + left.getClass().getName() + " and " +
                        right.getClass().getName());
    }

}
