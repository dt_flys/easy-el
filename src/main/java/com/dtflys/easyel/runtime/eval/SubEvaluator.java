package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.utils.TypeHelper;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElDate;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.runtime.EasyElTimeDuration;
import com.dtflys.easyel.runtime.wrapper.NumberWrapper;

import java.util.Date;

/**
 * 减法求值器
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class SubEvaluator {


    public Object evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {

        if (left instanceof NumberWrapper && right instanceof NumberWrapper) {
            return ((NumberWrapper) left).sub((NumberWrapper) right);
        }

        Class leftClass = left.getClass();
        Class rightClass = right.getClass();

        if (TypeHelper.isDate(leftClass)) {
            EasyElDate leftDate;
            if (left instanceof EasyElDate) {
                leftDate = (EasyElDate) left;
            } else {
                leftDate = new EasyElDate((Date) left);
            }

            if (TypeHelper.isDate(rightClass)) {
                EasyElDate rightDate;
                if (right instanceof EasyElDate) {
                    rightDate = (EasyElDate) right;
                } else {
                    rightDate = new EasyElDate((Date) right);
                }

                return leftDate.substract(rightDate);
            }

            if (TypeHelper.isTimeDuration(rightClass)) {
                return leftDate.substract((EasyElTimeDuration) right);
            }
        }


/*
        Class leftClass = left.getClass();
        Class rightClass = right.getClass();
        if (TypeHelper.isInteger(leftClass) || TypeHelper.isNumeric(leftClass)) {
            BigDecimal leftNum;
            if (left instanceof BigDecimal) {
                leftNum = (BigDecimal) left;
            } else {
                leftNum = new BigDecimal(left + "");
            }
            if (TypeHelper.isInteger(rightClass) || TypeHelper.isNumeric(rightClass)) {
                BigDecimal rightNum;
                if (right instanceof BigDecimal) {
                    rightNum = (BigDecimal) right;
                } else {
                    rightNum = new BigDecimal(right + "");
                }
                return leftNum.subtract(rightNum);
            }
        }

        if (TypeHelper.isDate(leftClass)) {
            EasyElDate leftDate;
            if (left instanceof EasyElDate) {
                leftDate = (EasyElDate) left;
            } else {
                leftDate = new EasyElDate((Date) left);
            }

            if (TypeHelper.isDate(rightClass)) {
                EasyElDate rightDate;
                if (right instanceof EasyElDate) {
                    rightDate = (EasyElDate) right;
                } else {
                    rightDate = new EasyElDate((Date) right);
                }

                return leftDate.substract(rightDate);
            }

            if (TypeHelper.isTimeDuration(rightClass)) {
                return leftDate.substract((EasyElTimeDuration) right);
            }
        }

        if (TypeHelper.isTimeDuration(leftClass)) {
            EasyElTimeDuration leftDuration = (EasyElTimeDuration) left;
            if (TypeHelper.isTimeDuration(rightClass)) {
                EasyElTimeDuration rightDuration = (EasyElTimeDuration) right;
                return leftDuration.substract(rightDuration);
            }
        }
*/

        throw new EasyElEvalException(context, node,
                "operator '-' do not support for type " + left.getClass().getName() + " and " +
                    right.getClass().getName());
    }

}
