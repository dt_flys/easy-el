package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.utils.MetaHelper;
import com.dtflys.easyel.utils.NumberHelper;
import com.dtflys.easyel.utils.TypeHelper;

/**
 * 取余求值器
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class RemainderEvaluator {

    public Object evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {
        left = MetaHelper.unwrap(left);
        right = MetaHelper.unwrap(right);
        Class leftClass = left.getClass();
        Class rightClass = right.getClass();
        if (TypeHelper.isNumber(leftClass) || TypeHelper.isNumber(leftClass)) {
            return NumberHelper.mod((Number) left, (Number) right);
/*
            BigDecimal leftNum;
            if (left instanceof BigDecimal) {
                leftNum = (BigDecimal) left;
            } else {
                leftNum = new BigDecimal(left + "");
            }
            if (TypeHelper.isInteger(rightClass) || TypeHelper.isNumeric(rightClass)) {
                BigDecimal rightNum;
                if (right instanceof BigDecimal) {
                    rightNum = (BigDecimal) right;
                } else {
                    rightNum = new BigDecimal(right + "");
                }
                return leftNum.remainder(rightNum);
            }
*/
        }
        throw new EasyElEvalException(context, node,
                "operator '%' do not support for type " + leftClass + " and " + rightClass);
    }

}
