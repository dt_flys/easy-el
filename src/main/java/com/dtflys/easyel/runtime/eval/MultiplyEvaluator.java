package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.runtime.wrapper.NumberWrapper;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MultiplyEvaluator {

    public Object evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {
        left = MetaHelper.wrap(left);
        right = MetaHelper.wrap(right);

        if (left instanceof NumberWrapper && right instanceof NumberWrapper) {
            return ((NumberWrapper) left).multi((NumberWrapper) right);
        }

        throw new EasyElEvalException(context, node,
                "operator '*' do not support for type " + left.getClass().getName() + " and " +
                        right.getClass().getName());
    }

}
