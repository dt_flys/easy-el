package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.runtime.wrapper.StringWrapper;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class RegexMatchEvaluator {

    public Boolean evaluate(EasyElRuntimeContext context,
                            ASTBinaryExpression node,
                            Object left, Object right) {
        left = MetaHelper.wrap(left);

        if (left instanceof StringWrapper
                && right instanceof CharSequence) {
            return ((StringWrapper) left).matches((CharSequence) right);
        }

        throw new EasyElEvalException(context, node,
                "operator '=~' do not support for type " + left.getClass().getName() + " and " +
                        right.getClass().getName());
    }
}
