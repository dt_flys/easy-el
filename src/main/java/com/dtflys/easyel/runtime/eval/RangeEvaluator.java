package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTRange;
import com.dtflys.easyel.runtime.EasyElRange;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class RangeEvaluator {

    public Object evaluate(EasyElRuntimeContext context, ASTRange node, Comparable from, Comparable to) {
        EasyElRange range = new EasyElRange(
                (Comparable) MetaHelper.unwrap(from),
                (Comparable) MetaHelper.unwrap(to),
                null, node.isIncludeFrom(), node.isIncludeTo());
        return range;
    }

}
