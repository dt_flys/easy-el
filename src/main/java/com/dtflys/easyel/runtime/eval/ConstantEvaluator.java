package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTConstant;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * 常量求值器
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ConstantEvaluator {

    public Object evaluate(EasyElRuntimeContext context, ASTConstant constant) {
        return MetaHelper.wrap(constant.getValue());
    }
}
