package com.dtflys.easyel.runtime.eval;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;
import com.dtflys.easyel.runtime.wrapper.NumberWrapper;
import com.dtflys.easyel.runtime.wrapper.StringWrapper;
import com.dtflys.easyel.utils.MetaHelper;

/**
 * 加法求值器
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class AddEvaluator  {

    public Object evaluate(EasyElRuntimeContext context, ASTBinaryExpression node, Object left, Object right) {

        left = MetaHelper.wrap(left);
        right = MetaHelper.wrap(right);

        if (left instanceof NumberWrapper && right instanceof NumberWrapper) {
            return ((NumberWrapper) left).add((NumberWrapper) right);
        }

        if (left instanceof StringWrapper || right instanceof StringWrapper) {
            return MetaHelper.wrap(left.toString() + right.toString());
        }

        throw new EasyElEvalException(context, node,
                "operator '+' do not support for type " + left.getClass().getName() + " and " +
                    right.getClass().getName());
    }
}
