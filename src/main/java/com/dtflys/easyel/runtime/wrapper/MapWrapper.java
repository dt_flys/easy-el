package com.dtflys.easyel.runtime.wrapper;

import com.dtflys.easyel.exception.EasyElAccessException;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MapWrapper extends Wrapper {


    public MapWrapper(Map value) {
        super(value, value.getClass());
    }

    public Map getMap() {
        return (Map) getOriginalObject();
    }

    @Override
    public Object access(String name) throws IllegalAccessException, EasyElAccessException, InvocationTargetException {
        try {
            return super.access(name);
        } catch (Throwable th) {
            Map map = getMap();
            if (map.containsKey(name)) {
                return map.get(name);
            }
            throw th;
        }
    }
}
