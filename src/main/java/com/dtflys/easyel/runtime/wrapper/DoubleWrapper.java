package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class DoubleWrapper extends NumberWrapper<Double> {

    public DoubleWrapper(Double value) {
        super(value, Double.class);
    }

    @Override
    public NumberWrapper negate() {
        return new DoubleWrapper((Double) this.getOriginalObject() * -1.0);
    }
}
