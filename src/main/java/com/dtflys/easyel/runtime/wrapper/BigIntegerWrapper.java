package com.dtflys.easyel.runtime.wrapper;

import java.math.BigInteger;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class BigIntegerWrapper extends NumberWrapper {

    public BigIntegerWrapper(BigInteger value) {
        super(value, BigInteger.class);
    }

    @Override
    public BigIntegerWrapper negate() {
        return new BigIntegerWrapper(((BigInteger) this.getOriginalObject()).negate());
    }
}
