package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class BooleanWrapper extends Wrapper {


    public BooleanWrapper(Boolean value) {
        super(value, Boolean.class);
    }

    public Boolean getBoolean() {
        return (Boolean) getOriginalObject();
    }

    public Boolean and(BooleanWrapper obj) {
        Boolean bool1 = getBoolean();
        Boolean bool2 = obj.getBoolean();
        return bool1 && bool2;
    }

    public Boolean or(BooleanWrapper obj) {
        Boolean bool1 = getBoolean();
        Boolean bool2 = obj.getBoolean();
        return bool1 || bool2;
    }

    public Boolean byteAnd(BooleanWrapper obj) {
        Boolean bool1 = getBoolean();
        Boolean bool2 = obj.getBoolean();
        return bool1 & bool2;
    }

    public Boolean byteOr(BooleanWrapper obj) {
        Boolean bool1 = getBoolean();
        Boolean bool2 = obj.getBoolean();
        return bool1 | bool2;
    }

}
