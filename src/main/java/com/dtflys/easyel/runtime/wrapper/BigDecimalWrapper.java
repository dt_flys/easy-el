package com.dtflys.easyel.runtime.wrapper;

import java.math.BigDecimal;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class BigDecimalWrapper extends NumberWrapper {

    public BigDecimalWrapper(BigDecimal value) {
        super(value, BigDecimal.class);
    }

    @Override
    public BigDecimalWrapper negate() {
        return new BigDecimalWrapper(((BigDecimal) this.getOriginalObject()).negate());
    }
}
