package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ShortWrapper extends NumberWrapper {

    public ShortWrapper(Short value) {
        super(value, Short.class);
    }

    @Override
    public ShortWrapper negate() {
        return new ShortWrapper((short) (((Short) this.getOriginalObject()).shortValue() * -1));
    }
}
