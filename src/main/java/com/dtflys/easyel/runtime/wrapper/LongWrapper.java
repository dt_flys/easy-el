package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class LongWrapper extends NumberWrapper {

    public LongWrapper(Long value) {
        super(value, Long.class);
    }

    @Override
    public LongWrapper negate() {
        return new LongWrapper((Long) this.getOriginalObject() * -1L);
    }
}
