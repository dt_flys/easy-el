package com.dtflys.easyel.runtime.wrapper;


/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class JavaBeanWrapper extends Wrapper {

    public JavaBeanWrapper(Object originalObject, Class originalClass) {
        super(originalObject, originalClass);
    }

}
