package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class FloatWrapper extends NumberWrapper {

    public FloatWrapper(Float value) {
        super(value, Float.class);
    }

    @Override
    public NumberWrapper negate() {
        return new FloatWrapper((Float) this.getOriginalObject() * -1.0f);
    }
}
