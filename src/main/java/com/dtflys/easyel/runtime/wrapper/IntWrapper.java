package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class IntWrapper extends NumberWrapper {

    public IntWrapper(Integer value) {
        super(value, Integer.class);
    }

    @Override
    public IntWrapper negate() {
        return new IntWrapper((Integer) this.getOriginalObject() * -1);
    }
}
