package com.dtflys.easyel.runtime.wrapper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class StringWrapper extends Wrapper implements Comparable<StringWrapper>, CharSequence {

    public StringWrapper(CharSequence value) {
        super(value.toString(), String.class);
    }

    public StringWrapper add(Object obj) {
        return new StringWrapper(getOriginalObject() + obj.toString());
    }

    @Override
    public int length() {
        return toString().length();
    }

    @Override
    public char charAt(int index) {
        return toString().charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return toString().substring(start, end);
    }

    @Override
    public String toString() {
        return (String) getOriginalObject();
    }

    @Override
    public int compareTo(StringWrapper obj) {
        String str1 = toString();
        String str2 = obj.toString();
        return str1.compareTo(str2);
    }

    public boolean matches(CharSequence pattern) {
        String val = toString();
        return val.matches(pattern.toString());
    }
}
