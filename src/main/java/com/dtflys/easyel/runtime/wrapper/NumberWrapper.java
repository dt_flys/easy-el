package com.dtflys.easyel.runtime.wrapper;


import com.dtflys.easyel.utils.MetaHelper;
import com.dtflys.easyel.utils.NumberHelper;

import java.math.BigDecimal;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class NumberWrapper<T extends Number> extends Wrapper implements Comparable<NumberWrapper> {

    public NumberWrapper(T originalObject, Class<T> originalClass) {
        super(originalObject, originalClass);
    }


    public abstract NumberWrapper negate();

    public Number getNumber() {
        return (Number) getOriginalObject();
    }

    @Override
    public int compareTo(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return NumberHelper.compare(number1, number2);
    }


    public NumberWrapper add(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return (NumberWrapper) MetaHelper.wrap(NumberHelper.add(number1, number2));
    }


    public NumberWrapper sub(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return (NumberWrapper) MetaHelper.wrap(NumberHelper.sub(number1, number2));
    }

    public NumberWrapper multi(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return (NumberWrapper) MetaHelper.wrap(NumberHelper.multi(number1, number2));
    }

    public NumberWrapper div(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return (NumberWrapper) MetaHelper.wrap(NumberHelper.div(number1, number2));
    }

    public NumberWrapper power(NumberWrapper obj) {
        Number number1 = getNumber();
        Number number2 = obj.getNumber();
        return (NumberWrapper) MetaHelper.wrap(NumberHelper.power(number1, number2));
    }

    public Integer intValue() {
        return getNumber().intValue();
    }

    public Long longValue() {
        return getNumber().longValue();
    }

    public Float floatValue() {
        return getNumber().floatValue();
    }

    public Double doubleValue() {
        return getNumber().doubleValue();
    }

    public BigDecimal decimalValue() {
        Number number = getNumber();
        if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        }
        return new BigDecimal(number.toString());
    }


}
