package com.dtflys.easyel.runtime;

import com.dtflys.easyel.exception.EasyElAccessException;
import com.dtflys.easyel.exception.EasyElAmbiguousMethodsException;

import java.lang.reflect.InvocationTargetException;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public interface EasyElObject {

    Object access(String name) throws IllegalAccessException, EasyElAccessException, InvocationTargetException;

    void inject(String name, Object value) throws IllegalAccessException, EasyElAccessException, InvocationTargetException;

    Object invokeMethod(String methodName, Object[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException;

    MetaClass getMetaClass();

}
