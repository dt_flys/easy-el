package com.dtflys.easyel.runtime;

import com.dtflys.easyel.compile.EasyElSource;

import java.util.Map;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElRuntimeContext {

    private EasyElRuntimeContext parent;

    private EasyElSource source;

    private Map<String, Object> env;

    private Object result;

    public EasyElRuntimeContext getParent() {
        return parent;
    }

    public void setParent(EasyElRuntimeContext parent) {
        this.parent = parent;
    }

    public Map<String, Object> getEnv() {
        return env;
    }

    public EasyElSource getSource() {
        if (source != null) {
            return source;
        }
        if (parent != null) {
            return parent.getSource();
        }
        return null;
    }

    public void setSource(EasyElSource source) {
        this.source = source;
    }

    public void setEnv(Map<String, Object> env) {
        this.env = env;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
