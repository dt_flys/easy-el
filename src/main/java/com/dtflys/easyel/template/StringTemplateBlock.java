package com.dtflys.easyel.template;

public class StringTemplateBlock {

    private final String prefix;

    private final char blockLeft;

    private final char blockRight;

    public StringTemplateBlock(String prefix) {
        this(prefix, '{', '}');
    }

    public StringTemplateBlock(String prefix, char blockLeft, char blockRight) {
        this.prefix = prefix;
        this.blockLeft = blockLeft;
        this.blockRight = blockRight;
    }

    public String getPrefix() {
        return prefix;
    }

    public char getBlockLeft() {
        return blockLeft;
    }

    public char getBlockRight() {
        return blockRight;
    }
}
