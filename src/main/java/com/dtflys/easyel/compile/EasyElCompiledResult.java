package com.dtflys.easyel.compile;

import com.dtflys.easyel.ast.ASTNode;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElCompiledResult {

    private final EasyElSource source;

    private final ASTNode astNode;


    public EasyElCompiledResult(EasyElSource source, ASTNode astNode) {
        this.source = source;
        this.astNode = astNode;
    }

    public EasyElSource getSource() {
        return source;
    }

    public ASTNode getAstNode() {
        return astNode;
    }
}
