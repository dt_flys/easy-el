package com.dtflys.easyel.compile;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElSourceLine {

    private final int lineNumber;
    private final StringBuilder text;

    public EasyElSourceLine(int lineNumber, String text) {
        this.lineNumber = lineNumber;
        this.text = new StringBuilder(text);
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getText() {
        return text.toString();
    }
    
    public char[] getChars() {
        return text.toString().toCharArray();
    }
    
    public void appendNL() {
        this.text.append('\n');
    }
}
