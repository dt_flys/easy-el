package com.dtflys.easyel.compile;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElParseError {

    private EasyElSourcePosition position;

    private String msg;

    public EasyElParseError(EasyElSourcePosition position, String msg) {
        this.position = position;
        this.msg = msg;
    }

    public EasyElSourcePosition getPosition() {
        return position;
    }

    public String getMsg() {
        return msg;
    }
}
