package com.dtflys.easyel.compile;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElCompileConfiguration {

    // 最大解析错误数: 在解析表达式源码时，当错误数量达到这个值时，会中断解析直接报错
    private Integer maxParseErrorNumber = 10;

    // 源代码名称
    private String sourceName = "[SCRIPT]";


    // 默认已加载的Class
    private Map<String, Class> importedClasses = new HashMap<>();

    public EasyElCompileConfiguration() {
    }

    public EasyElCompileConfiguration(Integer maxParseErrorNumber, Map<String, Class> importedClasses) {
        this.maxParseErrorNumber = maxParseErrorNumber;
        this.importedClasses = importedClasses;
    }

    public Integer getMaxParseErrorNumber() {
        return maxParseErrorNumber;
    }

    public void setMaxParseErrorNumber(Integer maxParseErrorNumber) {
        this.maxParseErrorNumber = maxParseErrorNumber;
    }

    public Map<String, Class> getImportedClasses() {
        return importedClasses;
    }

    public void setImportedClasses(Map<String, Class> importedClasses) {
        this.importedClasses = importedClasses;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void importClass(String name, Class clazz) {
        this.importedClasses.put(name, clazz);
    }
}
