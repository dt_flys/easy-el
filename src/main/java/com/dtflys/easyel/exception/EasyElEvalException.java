package com.dtflys.easyel.exception;

import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.compile.EasyElSourcePosition;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElEvalException extends EasyElRuntimeException {


    private static EasyElSourcePosition getPosition(ASTNode node) {
        return new EasyElSourcePosition(
                node.getStartLineNumber(),
                node.getEndLineNumber(),
                node.getStartColumnNumber(),
                node.getEndColumnNumber()
        );
    }


    private static <T extends ASTNode> EasyElSourcePosition getPosition(List<T> nodeList) {
        return new EasyElSourcePosition(
                nodeList.get(0).getStartLineNumber(),
                nodeList.get(nodeList.size() - 1).getEndLineNumber(),
                nodeList.get(0).getStartColumnNumber(),
                nodeList.get(nodeList.size() - 1).getEndColumnNumber()
        );
    }

    public <T extends ASTNode> EasyElEvalException(EasyElRuntimeContext context, List<T> nodeList, String msg) {
        this(context, nodeList, msg, null);
    }


    public <T extends ASTNode> EasyElEvalException(EasyElRuntimeContext context, List<T> nodeList, String msg, Throwable cause) {
        super(context, getPosition(nodeList), msg, cause);
    }


    public EasyElEvalException(EasyElRuntimeContext context, ASTNode node, String msg) {
        this(context, node, msg, null);
    }

    public EasyElEvalException(EasyElRuntimeContext context, ASTNode node, String msg, Throwable cause) {
        super(context, getPosition(node), msg, cause);
    }
}
