package com.dtflys.easyel.exception;

public class EasyElStringTemplateException extends RuntimeException {

    public EasyElStringTemplateException(String message) {
        super(message);
    }

    public EasyElStringTemplateException(String message, Throwable cause) {
        super(message, cause);
    }
}
