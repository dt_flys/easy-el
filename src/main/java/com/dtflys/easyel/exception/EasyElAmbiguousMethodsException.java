package com.dtflys.easyel.exception;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElAmbiguousMethodsException extends Exception {

    public EasyElAmbiguousMethodsException(String message) {
        super(message);
    }
}
