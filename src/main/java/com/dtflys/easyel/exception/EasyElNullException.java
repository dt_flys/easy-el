package com.dtflys.easyel.exception;

import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElNullException extends EasyElEvalException {


    public EasyElNullException(EasyElRuntimeContext context, ASTNode node) {
        super(context, node, "Null Exception");
    }

}
