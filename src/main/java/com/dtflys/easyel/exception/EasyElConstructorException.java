package com.dtflys.easyel.exception;

import com.dtflys.easyel.utils.MetaHelper;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElConstructorException extends Exception {

    public EasyElConstructorException(Class clazz, Object[] arguments) {
        super("Can not resolve constructor " + clazz.getSimpleName() + MetaHelper.toArgumentTypesString(arguments));
    }
}
