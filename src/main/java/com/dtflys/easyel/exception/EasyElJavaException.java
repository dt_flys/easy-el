package com.dtflys.easyel.exception;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElJavaException extends RuntimeException {

    public EasyElJavaException(String message) {
        super(message);
    }
}
