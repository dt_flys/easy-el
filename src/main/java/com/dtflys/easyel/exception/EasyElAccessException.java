package com.dtflys.easyel.exception;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElAccessException extends Exception {
    public EasyElAccessException(Class clazz, String accessName) {
        super( "No field/property \"" + accessName + "\"" +
                " or method \"" + accessName + "()\" for class \"" + clazz.getName() + "\"");
    }
}
