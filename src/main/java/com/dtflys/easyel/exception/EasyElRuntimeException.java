package com.dtflys.easyel.exception;


import com.dtflys.easyel.compile.EasyElSourcePosition;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElRuntimeException extends EasyElException {

    public EasyElRuntimeException(EasyElRuntimeContext context, EasyElSourcePosition position, String msg) {
        super(false, getErrorMsg(context, position, msg));
    }


    public EasyElRuntimeException(EasyElRuntimeContext context, EasyElSourcePosition position, String msg, Throwable cause) {
        super(false, getErrorMsg(context, position, msg), cause);
    }

}
