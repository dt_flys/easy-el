package com.dtflys.easyel.exception;

import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.runtime.EasyElRuntimeContext;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElNoSuchMethodException extends EasyElEvalException {

    public EasyElNoSuchMethodException(EasyElRuntimeContext context, ASTNode node, String methodName) {
        super(context, node, buildMessage(methodName));
    }

    private static String buildMessage(String methodName) {
        return "No such method \"" + methodName + "\"";
    }
}
