package com.dtflys.easyel.exception;

import com.dtflys.easyel.compile.EasyElParseError;
import com.dtflys.easyel.compile.EasyElSource;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElParseException extends EasyElException {

    public EasyElParseException(EasyElSource source, List<EasyElParseError> errors) {
        super(false, getErrorMsg(source, errors));
    }

    protected static String getErrorMsg(EasyElSource source, List<EasyElParseError> errors) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n");
        int len = errors.size();
        for (int i = 0; i < len; i++) {
            EasyElParseError error = errors.get(i);
            String errMsg = "  " + getEasyElMark()
                    + " error[parse]: "
                    + error.getMsg()
                    + "\n\n"
                    + getIndentSpace(9)
                    + getBody(source, error.getPosition());
            builder.append(errMsg);
            if (i < len - 1) {
                builder.append("    ...");
                builder.append("\n\n");
            }
        }
        return builder.toString();
    }

}
