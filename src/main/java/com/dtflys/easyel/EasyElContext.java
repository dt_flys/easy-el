package com.dtflys.easyel;

import com.dtflys.easyel.compile.EasyElCompileConfiguration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class EasyElContext {


    private EasyElCompileConfiguration compileConfiguration = new EasyElCompileConfiguration();

    private Map<String, Object> env = new HashMap<>();

    public Map<String, Object> getEnv() {
        return env;
    }

    public void setEnv(Map<String, Object> env) {
        this.env = env;
    }

    public void setEnv(String name, Object value) {
        this.env.put(name, value);
    }

    public EasyElCompileConfiguration getCompileConfiguration() {
        return compileConfiguration;
    }

    public void importClass(String name, Class clazz) {
        compileConfiguration.importClass(name, clazz);
    }


    public EasyElContext() {
        importDefaultClasses();
    }

    private void importDefaultClasses() {
        importClass("short", short.class);
        importClass("Short", Short.class);
        importClass("int", int.class);
        importClass("Integer", Integer.class);
        importClass("long", long.class);
        importClass("Long", Long.class);
        importClass("BigInteger", BigInteger.class);
        importClass("float", float.class);
        importClass("Float", Float.class);
        importClass("double", double.class);
        importClass("Double", Double.class);
        importClass("BigDecimal", BigDecimal.class);
        importClass("char", char.class);
        importClass("Character", Character.class);
        importClass("String", String.class);
        importClass("Math", Math.class);
    }
}
