package com.dtflys.easyel.parser;

import com.dtflys.easyel.compile.EasyElSource;

public class  EToken {

    public static final int
            EOF=0, DOT=1, DOUBLE_DOT=2, DOUBLE_STAR=3, QUESTION=4, DOUBLE_QUESTION=5, COMMA=6, COLON=7, SEMICOLON=8,
            NEW=9, TRUE=10, FALSE=11, NULL=12, ASSIGN=13, GT=14, LT=15, NOT=16, J_NOT=17, GE=18,
            LE=19, EQ=20, NOT_EQ=21, BIT_AND=22, BIT_OR=23, AND=24, OR=25, J_AND=26,
            J_OR=27, PLUS=28, PLUS_PLUS=29, PLUS_ASSIGN =30, MINUS=31, MINUS_MINUS=32, MINUS_ASSIGN =33,
            MUL=34, DIV=35, MOD=36, POWER=37,REGEX_PATTERN=38, REGEX_MATCH=39, NOT_REGEX_MATCH=40,
            LPAREN=41, RPAREN=42, LBRACE=43, RBRACE=44, LBRACK=45, RBRACK=46, DATE_YYYY_MM_DD=47,
            DATE_HH_MM=48, DATE_HH_MM_SS=49, DATE_HH_MM_SS_ss=50, DOUBLE_QUOTE_STRING =51, SINGLE_QUOTE_STRING=52,
            INTEGER=53, LONG=54, NUMERIC=55, FLOAT=56, DOUBLE=57, DECIMAL=58, CAPITALIZED_IDENTIFIER=59,
            HEX=60, OCT=61, IDENTIFIER=62, WS=63, NL=64, SINGLE_LINE_COMMENT=65, MULTIPLE_LINES_COMMENT=66,
            RIGHT_ARROW=67,
    
            IN=68, NOT_IN=69, IF=70, ELSE=71, FOR=72, DO=73, WHILE=74, BREAK=75, CONTINUE=76, RETURN=77,
            LET=78, VAR=79;

    
    private static String[] makeSymbolicNames() {
        return new String[] {
                "EOF", "DOT", "DOUBLE_DOT", "DOUBLE_STAR", "QUESTION", "DOUBLE_QUESTION", "COMMA", "COLON", "SEMICOLON",
                "NEW", "TRUE", "FALSE", "NULL", "ASSIGN", "GT", "LT", "NOT", "J_NOT",
                "GE", "LE", "EQ", "NOT_EQ", "BIT_AND", "BIT_OR", "AND", "OR", "J_AND",
                "J_OR", "PLUS", "PLUS_PLUS", "PLUS_ASSIGN", "MINUS", "MINUS_MINUS", "MINUS_ASSIGN",
                "MUL", "DIV", "MOD", "POWER", "REGEX_PATTERN", "REGEX_MATCH", "NOT_REGEX_MATCH",
                "LPAREN", "RPAREN", "LBRACE", "RBRACE", "LBRACK", "RBRACK", "DATE_YYYY_MM_DD",
                "DATE_HH_MM", "DATE_HH_MM_SS", "DATE_HH_MM_SS_ss", "DOUBLE_QUOTE_STRING", "SINGLE_QUOTE_STRING",
                "INTEGER", "LONG", "NUMERIC","FLOAT", "DOUBLE", "DECIMAL", "CAPITALIZED_IDENTIFIER",
                "HEX", "OCT", "IDENTIFIER", "WS", "NL", "SINGLE_LINE_COMMENT", "MULTIPLE_LINES_COMMENT",
                "RIGHT_ARROW",

                "IN", "NOT_IN", "IF", "ELSE", "FOR", "DO", "WHILE", "BREAK", "CONTINUE", "RETURN",
                "LET", "VAR",
        };
    }
    private static final String[] SYMBOLIC_NAMES = makeSymbolicNames();
    
    EasyElSource source;

    int type;
    
    String symbolicName;
    
    String text;
    
    int line;
    
    int startColumn;
    
    int endColumn;
    
    boolean skip = false;
    
    public EToken(int type, ELexer.TokenContext context) {
        this(context.source, type, context.getText(), context.line, context.startColumn, context.endColumn);
    }

    public EToken(EasyElSource source, int type, String text, int line, int startColumn, int endColumn) {
        this.source = source;
        this.type = type;
        this.symbolicName = SYMBOLIC_NAMES[type];
        this.text = text;
        this.line = line;
        this.startColumn = startColumn;
        this.endColumn = endColumn;
    }
    
    public EToken skip() {
        this.skip = true;
        return this;
    }

    public EasyElSource getSource() {
        return source;
    }

    public int getType() {
        return type;
    }

    public String getSymbolicName() {
        return symbolicName;
    }

    public String getText() {
        return text;
    }

    public int getLine() {
        return line;
    }

    public int getStartColumn() {
        return startColumn;
    }

    public int getEndColumn() {
        return endColumn;
    }

    public boolean isSkip() {
        return skip;
    }

    @Override
    public String toString() {
        switch (type) {
            case INTEGER:
            case DOUBLE:
            case DECIMAL:
            case HEX:
            case OCT:
            case DOUBLE_QUOTE_STRING:
            case SINGLE_QUOTE_STRING:
            case IDENTIFIER:
                return text;
            default:
                return "<" + symbolicName + ">";
        }
    }
}
