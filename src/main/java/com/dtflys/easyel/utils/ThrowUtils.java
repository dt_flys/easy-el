package com.dtflys.easyel.utils;

import java.util.LinkedList;
import java.util.List;


/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ThrowUtils {

    public static void throwWithoutEasyElPackages(RuntimeException ex) {

        StackTraceElement[] stack = ex.getStackTrace();
        List<StackTraceElement> newStackList = new LinkedList<>();
        for (StackTraceElement stackTraceElement : stack) {
            if (!stackTraceElement.getClassName().startsWith("com.dtflys.easyel.compile") &&
//                    !stackTraceElement.getClassName().startsWith("com.dtflys.easyel.ast") &&
//                    !stackTraceElement.getClassName().startsWith("com.dtflys.easyel.runtime") &&
                    !stackTraceElement.getClassName().startsWith("com.dtflys.easyel.EasyEl")) {
                newStackList.add(stackTraceElement);
            }
        }
        StackTraceElement[] newStack = new StackTraceElement[newStackList.size()];
        for (int i = 0; i < newStackList.size(); i++) {
            StackTraceElement stackTraceElement = newStackList.get(i);
            newStack[i] = stackTraceElement;
        }
        ex.setStackTrace(newStack);
//        AnsiHelper.startAnsiConsole();
        throw ex;
    }


}
