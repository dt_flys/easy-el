package com.dtflys.easyel.utils;

import com.dtflys.easyel.runtime.EasyElDate;
import com.dtflys.easyel.runtime.EasyElTimeDuration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TypeHelper {

    public static boolean isJavaByte(Class clazz) {
        return byte.class.isAssignableFrom(clazz) || Byte.class.isAssignableFrom(clazz);
    }


    public static boolean isJavaInteger(Class clazz) {
        return int.class.isAssignableFrom(clazz) || Integer.class.isAssignableFrom(clazz);
    }

    public static boolean isJavaLong(Class clazz) {
        return long.class.isAssignableFrom(clazz) || Long.class.isAssignableFrom(clazz);
    }

    public static boolean isJavaShort(Class clazz) {
        return short.class.isAssignableFrom(clazz) || Short.class.isAssignableFrom(clazz);
    }

    public static boolean isJavaFloat(Class clazz) {
        return float.class.isAssignableFrom(clazz) || Float.class.isAssignableFrom(clazz);
    }

    public static boolean isJavaDouble(Class clazz) {
        return double.class.isAssignableFrom(clazz) || Double.class.isAssignableFrom(clazz);
    }

    public static boolean isJavaDate(Class clazz) {
        return Date.class.isAssignableFrom(clazz);
    }

    public static boolean isBigInteger(Class clazz) {
        return BigInteger.class.isAssignableFrom(clazz);
    }

    public static boolean isBigDecimal(Class clazz) {
        return BigDecimal.class.isAssignableFrom(clazz);
    }

    public static boolean isCharSequence(Class clazz) {
        return CharSequence.class.isAssignableFrom(clazz);
    }

    public static boolean isBoolean(Class clazz) {
        return boolean.class.isAssignableFrom(clazz) || Boolean.class.isAssignableFrom(clazz);
    }

    public static boolean isInteger(Class clazz) {
        return isJavaShort(clazz) || isJavaInteger(clazz) || isJavaLong(clazz) || isBigInteger(clazz);
    }

    public static boolean isNumeric(Class clazz) {
        return isJavaFloat(clazz) || isJavaDouble(clazz) || isBigDecimal(clazz);
    }

    public static boolean isNumber(Class clazz) {
        return isInteger(clazz) || isNumeric(clazz);
    }

    public static boolean isDate(Class clazz) {
        return isJavaDate(clazz) || EasyElDate.class.isAssignableFrom(clazz);
    }

    public static boolean isTimeDuration(Class clazz) {
        return EasyElTimeDuration.class.isAssignableFrom(clazz);
    }

    public static boolean isString(Class clazz) {
        return isCharSequence(clazz);
    }
}
