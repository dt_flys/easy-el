/*
package com.dtflys.easyel.utils;

import org.apache.commons.lang3.StringUtils;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.fusesource.jansi.AnsiPrintStream;
import org.fusesource.jansi.internal.CLibrary;

import java.io.PrintStream;
import java.util.Locale;
import java.util.Properties;

*/
/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 *//*

public class AnsiHelper {

    private static String OS_NAME = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);

    private static final boolean IS_WINDOWS = OS_NAME.contains("win");

    private static final boolean IS_CYGWIN = IS_WINDOWS
            && System.getenv("PWD") != null
            && System.getenv("PWD").startsWith("/")
            && !"cygwin".equals(System.getenv("TERM"));

    private static final boolean IS_MINGW_XTERM = IS_WINDOWS
            && System.getenv("MSYSTEM") != null
            && System.getenv("MSYSTEM").startsWith("MINGW")
            && "xterm".equals(System.getenv("TERM"));

    private static final boolean IS_LINUX = OS_NAME.contains("linux");


    private static boolean IS_IDEA = isIDEA();

    private static boolean IS_ECLIPSE = isEclipse();

    private static boolean IS_A_TTY = isatty();

    private static boolean IS_MAVEN_CMD = isMavenCmd();

    private static boolean ENABLE_ANSI = false;

    private static boolean IS_NEED_PASSTHROUGH = isNeedPassthrough();


    private static boolean isIDEA() {

//        Properties properties = System.getProperties();

//        AnsiConsole.systemInstall();

//        System.out.println("\n\n!!!!!!!\n!!!" + properties + "\n!!!!!!!!\n\n");

        String jbossPkgs = System.getProperty("jboss.modules.system.pkgs");
        if (StringUtils.isNotBlank(jbossPkgs)) {
            if (jbossPkgs.indexOf("intellij") > -1) {
                return true;
            }
        }
        String classPath = System.getProperty("java.class.path");
        if (classPath.contains("idea_rt")
                || classPath.contains("IntelliJ")) {
            return true;
        }
        return false;
    }


    private static boolean isEclipse() {
        String classPath = System.getProperty("java.class.path");
        boolean ret = classPath.contains("eclipse");
        return ret;
    }

    private static boolean isatty() {
        int isatty = CLibrary.isatty(CLibrary.STDERR_FILENO);
        return isatty > 0;
    }

    private static boolean isMavenCmd() {
        return System.out.getClass().getName().indexOf("maven") > -1;
    }

    private static boolean isEnableAnsi() {
        if (IS_IDEA || IS_A_TTY) return true;
        if (IS_ECLIPSE) return false;
        return !IS_MAVEN_CMD;
//        String classPath = System.getProperty("java.class.path");
//        if (classPath.contains("junit")) return false;
//        return IS_MAVEN_CMD;
    }


    public static boolean isNeedPassthrough() {
        if (IS_IDEA) {
//            System.setProperty("jansi.passthrough", "true");
            return true;
        }
        return false;
    }

    public static String bold() {
        if (!ENABLE_ANSI) {
            return "";
        }
        return Ansi.ansi().bold().toString();
    }

    public static String fg(Ansi.Color color) {
        if (!ENABLE_ANSI) {
            return "";
        }
        return Ansi.ansi().fg(color).toString();
    }

    public static String bg(Ansi.Color color) {
        if (!ENABLE_ANSI) {
            return "";
        }
        return Ansi.ansi().bg(color).toString();
    }

    public static String reset() {
        if (!ENABLE_ANSI) {
            return "";
        }
        if (IS_NEED_PASSTHROUGH) {
            return ansi(0);
        }
        return Ansi.ansi().reset().toString();
    }

    public static void startAnsiConsole() {
        if (!IS_IDEA) {
            AnsiConsole.systemInstall();
        }
    }

    public static void stopAnsiConsole() {
        if (!IS_IDEA) {
            AnsiConsole.systemUninstall();
        }
    }

    public static String ansi(int colorNumber) {
        if (ENABLE_ANSI) {
            if (IS_WINDOWS && !(IS_CYGWIN || IS_MINGW_XTERM)) {
                return "\u001b[" + colorNumber + "m";
            } else if (!IS_LINUX) {
                return "\033[" + colorNumber + "m";
        }
        }
        return "";
    }

}
*/
