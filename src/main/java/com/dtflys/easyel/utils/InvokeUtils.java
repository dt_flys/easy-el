package com.dtflys.easyel.utils;


import com.dtflys.easyel.exception.EasyElAmbiguousMethodsException;
import com.dtflys.easyel.runtime.EasyElObject;
import com.dtflys.easyel.runtime.MetaClass;
import com.dtflys.easyel.runtime.MetaClassFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class InvokeUtils {

    public static Object invoke(Object obj, String methodName, Object[] args)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException {
        if (obj instanceof EasyElObject) {
            return ((EasyElObject) obj).invokeMethod(methodName, args);
        }
        Class clazz = obj.getClass();
        MetaClass metaClass = MetaClassFactory.getMetaClass(clazz);
        return metaClass.invokeMethod(obj, methodName, args);
    }


    public static Object invokeStatic(Class clazz, String methodName, Object[] args)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, EasyElAmbiguousMethodsException {
        MetaClass metaClass = MetaClassFactory.getMetaClass(clazz);
        return metaClass.invokeStaticMethod(methodName, args);
    }

}


