package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTIdentifier;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class IdentifierTransformer extends ExpressionTransformer {

    public ASTIdentifier transform(Token token) {
        ASTIdentifier identifier = new ASTIdentifier(token.getText());
        setPosition(identifier, token);
        printNode(identifier);
        return identifier;
    }

}
