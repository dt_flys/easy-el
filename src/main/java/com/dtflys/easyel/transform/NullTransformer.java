package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTNull;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class NullTransformer extends ExpressionTransformer {

    public ASTNull transform(Token token) {
        ASTNull nullNode = new ASTNull();
        setPosition(nullNode, token);
        printNode(nullNode);
        return nullNode;
    }

}
