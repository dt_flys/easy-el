package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTNode;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class Transformer {

    protected void setPosition(ASTNode node, int startLine, int endLine, int startColumn, int endColumn) {
        node.setStartLineNumber(startLine);
        node.setEndLineNumber(endLine);
        node.setStartColumnNumber(startColumn);
        node.setEndColumnNumber(endColumn);
    }


    protected void setPosition(ASTNode node, Token startToken, Token endToken) {
        setPosition(
                node,
                startToken.getLine(),
                endToken.getLine(),
                startToken.getCharPositionInLine(),
                endToken.getCharPositionInLine() + endToken.getText().length());
    }


    protected void setPosition(ASTNode node, Token startToken, ASTNode endNode) {
        setPosition(
                node,
                startToken.getLine(),
                endNode.getEndLineNumber(),
                startToken.getCharPositionInLine(),
                endNode.getEndColumnNumber());
    }


    protected void setPosition(ASTNode node, Token token) {
        setPosition(node, token, token);
    }

    protected void setPosition(ASTNode node, ASTNode startNode, ASTNode endNode) {
        setPosition(
                node,
                startNode.getStartLineNumber(),
                endNode.getEndLineNumber(),
                startNode.getStartColumnNumber(),
                endNode.getEndColumnNumber());

    }
}
