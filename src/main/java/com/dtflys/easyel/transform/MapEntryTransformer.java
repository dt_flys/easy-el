package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTMapEntry;
import org.antlr.v4.runtime.Token;


/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MapEntryTransformer extends ExpressionTransformer {

    public ASTMapEntry transform(ASTExpression labelExpr, ASTExpression valueExpr, Token startToken, Token endToken) {
        ASTMapEntry entry = new ASTMapEntry(labelExpr, valueExpr);
        setPosition(entry, startToken, endToken);
        return entry;
    }

}
