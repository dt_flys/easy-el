package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTIdentifier;
import com.dtflys.easyel.ast.ASTListGenerator;
import org.antlr.v4.runtime.Token;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ListGeneratorTransformer extends ExpressionTransformer {

    public ASTListGenerator transform(ASTExpression itemExpr,
                                      List<ASTIdentifier> nameList,
                                      ASTExpression forConditionRight,
                                      Token startToken, Token endToken) {
        ASTListGenerator listGenerator = new ASTListGenerator(itemExpr, nameList, forConditionRight);
        setPosition(listGenerator, startToken, endToken);
        printNode(listGenerator);
        return listGenerator;
    }

}
