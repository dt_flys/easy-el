package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTNegativeExpression;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class NegativeExpressionTransformer extends ExpressionTransformer {

    public ASTNegativeExpression transform(ASTExpression expression, Token startToken, Token endToken) {
        ASTNegativeExpression negativeExpression = new ASTNegativeExpression(expression.getType(), expression);
        setPosition(negativeExpression, startToken, endToken);
        printNode(negativeExpression);
        return negativeExpression;
    }

}
