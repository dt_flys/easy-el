package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTMap;
import com.dtflys.easyel.ast.ASTMapEntry;
import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class MapTransformer extends ExpressionTransformer {

    public ASTMap transform(List<ASTMapEntry> mapEntries, Token startToken, Token endToken) {
        if (mapEntries == null) {
            mapEntries = new ArrayList<>();
        }
        ASTMap map = new ASTMap(mapEntries);
        setPosition(map, startToken, endToken);
        printNode(map);
        return map;
    }

}
