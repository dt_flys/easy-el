package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.ast.ASTTernaryExpression;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TernaryExpressionTransformer extends ExpressionTransformer {

    public ASTTernaryExpression transform(
            ASTExpression con,
            ASTExpression tb,
            ASTExpression tf) {
        ASTTernaryExpression ternaryExpression = new ASTTernaryExpression(con, tb, tf);
        ASTNode startNode = tb;
        ASTNode endNode = tf;
        if (tb == null && tf != null) {
            startNode = tf;
        } else if (tf == null && tb != null) {
            endNode = tb;
        }
        setPosition(ternaryExpression, startNode, endNode);
        printNode(ternaryExpression);
        return ternaryExpression;
    }

}
