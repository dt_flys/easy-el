package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTNotExpression;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class NotExpressionTransformer extends ExpressionTransformer {

    public ASTNotExpression transform(ASTExpression expression, Token startToken, Token endToken) {
        ASTNotExpression notExpression = new ASTNotExpression(expression);
        setPosition(notExpression, startToken, endToken);
        printNode(notExpression);
        return notExpression;
    }

}
