package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTAccessExpression;
import com.dtflys.easyel.ast.ASTConstant;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class AccessExpressionTransformer extends ExpressionTransformer {

    public ASTAccessExpression transform(Token dotToken, ASTConstant nameNode) {
        ASTAccessExpression accessExpression = new ASTAccessExpression(nameNode);
        setPosition(accessExpression, dotToken, nameNode);
        return accessExpression;
    }
}
