package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTList;
import org.antlr.v4.runtime.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class ListTransformer extends ExpressionTransformer {

    public ASTList transform(List<ASTExpression> listItems, Token startToken, Token endToken) {
        if (listItems == null) {
            listItems = new ArrayList<>();
        }
        ASTList list = new ASTList(listItems);
        setPosition(list, startToken, endToken);
        printNode(list);
        return list;
    }

}
