package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTInvokeExpression;
import org.antlr.v4.runtime.Token;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class InvokeExpressionTransformer extends ExpressionTransformer {

    public ASTInvokeExpression transform(List<ASTExpression> arguments, Token startToken, Token endToken) {
        ASTInvokeExpression invokeExpression = new ASTInvokeExpression(arguments);
        setPosition(invokeExpression, startToken, endToken);
        return invokeExpression;
    }

}
