package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTType;
import com.dtflys.easyel.compile.EasyElCompileConfiguration;
import org.antlr.v4.runtime.Token;

import java.util.Map;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TypeTransformer extends Transformer {

    public ASTType transform(Token token, EasyElCompileConfiguration compileConfiguration) {
        String name = token.getText();
        Map<String, Class> classMap = compileConfiguration.getImportedClasses();
        Class clazz = classMap.get(name);
        if (clazz == null) {
            return null;
        }
        ASTType type = new ASTType(name, clazz);
        setPosition(type, token);
        return type;
    }

}
