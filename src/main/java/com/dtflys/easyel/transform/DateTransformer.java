package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTConstant;
import com.dtflys.easyel.ast.ASTDate;
import com.dtflys.easyel.ast.ASTType;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class DateTransformer extends ExpressionTransformer {

    public ASTDate transform(Token yyyyMMddToken, Token timeToken, Token startToken, Token endToken) {
        String yyyyMMdd = yyyyMMddToken.getText();
        String time = null;
        if (timeToken != null) {
            time = timeToken.getText();
        }
        String year = yyyyMMdd.substring(0, 4);
        String month = yyyyMMdd.substring(5, 7);
        String day = yyyyMMdd.substring(8, 10);

        ASTConstant yearExpr = new ASTConstant(Integer.parseInt(year), ASTType.INTEGER);
        ASTConstant monthExpr = new ASTConstant(Integer.parseInt(month), ASTType.INTEGER);
        ASTConstant dayExpr = new ASTConstant(Integer.parseInt(day), ASTType.INTEGER);

        setPosition(yearExpr, startToken.getLine(), startToken.getLine(),
                startToken.getStartIndex(), startToken.getStartIndex() + 3);
        setPosition(monthExpr, startToken.getLine(), startToken.getLine(),
                startToken.getStartIndex() + 5, startToken.getStartIndex() + 6);
        setPosition(monthExpr, startToken.getLine(), startToken.getLine(),
                startToken.getStartIndex() + 8, startToken.getStartIndex() + 9);

        ASTDate date = new ASTDate();
        date.setYear(yearExpr);
        date.setMonth(monthExpr);
        date.setDay(dayExpr);

        if (time != null) {
            ASTConstant hoursExpr;
            ASTConstant minutesExpr;
            ASTConstant secondsExpr;
            ASTConstant millisecondsExpr;

            String[] timeArgs = time.split(":");
            String hh = timeArgs[0];
            String mm = timeArgs[1];
            Integer hours = Integer.parseInt(hh);
            Integer minutes = Integer.parseInt(mm);
            Integer seconds = null;
            Integer milliseconds = null;

            hoursExpr = new ASTConstant(hours, ASTType.INTEGER);
            minutesExpr = new ASTConstant(minutes, ASTType.INTEGER);
            setPosition(hoursExpr, timeToken.getLine(), timeToken.getLine(),
                    timeToken.getStartIndex(), timeToken.getStartIndex() + 1);
            setPosition(minutesExpr, timeToken.getLine(), timeToken.getLine(),
                    timeToken.getStartIndex() + 3, timeToken.getStartIndex() + 4);
            date.setHours(hoursExpr);
            date.setMinutes(minutesExpr);

            if (timeArgs.length > 2) {
                String ss = timeArgs[2];
                seconds = Integer.parseInt(ss);
                secondsExpr = new ASTConstant(seconds, ASTType.INTEGER);
                setPosition(secondsExpr, timeToken.getLine(), timeToken.getLine(),
                        timeToken.getStartIndex() + 6, timeToken.getStartIndex() + 7);
                date.setSeconds(secondsExpr);
                if (timeArgs.length > 3) {
                    String SS = timeArgs[3];
                    milliseconds = Integer.parseInt(SS);
                    millisecondsExpr = new ASTConstant(milliseconds, ASTType.INTEGER);
                    setPosition(millisecondsExpr, timeToken.getLine(), timeToken.getLine(),
                            timeToken.getStartIndex() + 10, timeToken.getStartIndex() + 11);
                    date.setMilliseconds(millisecondsExpr);
                }
            }

        }
        setPosition(date, startToken, endToken);
        printNode(date);
        return date;
    }

}
