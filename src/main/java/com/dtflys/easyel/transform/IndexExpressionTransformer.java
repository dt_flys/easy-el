package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTIndexExpression;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class IndexExpressionTransformer extends ExpressionTransformer {

    public ASTIndexExpression transform(ASTExpression index, Token startToken, Token endToken) {
        ASTIndexExpression indexExpression = new ASTIndexExpression(index);
        setPosition(index, startToken, endToken);
        return indexExpression;
    }


}
