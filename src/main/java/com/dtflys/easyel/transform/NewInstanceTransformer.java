package com.dtflys.easyel.transform;

import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTNewInstance;
import com.dtflys.easyel.ast.ASTType;
import org.antlr.v4.runtime.Token;

import java.util.List;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class NewInstanceTransformer extends ExpressionTransformer {

    public ASTNewInstance transform(ASTType type, List<ASTExpression> argumentExprs,
                                    Token startToken, Token endToken) {
        ASTNewInstance instance = new ASTNewInstance(type, argumentExprs);
        setPosition(instance, startToken, endToken);
        return instance;
    }

}
