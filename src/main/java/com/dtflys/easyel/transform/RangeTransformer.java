package com.dtflys.easyel.transform;

import com.dtflys.easyel.antlr.EasyElParser;
import com.dtflys.easyel.ast.ASTExpression;
import com.dtflys.easyel.ast.ASTRange;
import org.antlr.v4.runtime.Token;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class RangeTransformer extends ExpressionTransformer {

    public ASTRange transform(ASTExpression from, ASTExpression to, Token leftToken, Token rightToken) {
        boolean includeFrom = leftToken.getType() == EasyElParser.LBRACK;
        boolean includeTo = rightToken.getType() == EasyElParser.RBRACK;
        ASTRange range = new ASTRange(from, to, includeFrom, includeTo);
        setPosition(range, leftToken, rightToken);
        printNode(range);
        return range;
    }
}
