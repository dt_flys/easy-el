package com.dtflys.easyel;

import com.dtflys.easyel.ast.ASTBinaryExpression;
import com.dtflys.easyel.ast.ASTDate;
import com.dtflys.easyel.ast.ASTIdentifier;
import com.dtflys.easyel.ast.ASTIndexExpression;
import com.dtflys.easyel.ast.ASTInvokeExpression;
import com.dtflys.easyel.ast.ASTList;
import com.dtflys.easyel.ast.ASTListGenerator;
import com.dtflys.easyel.ast.ASTMap;
import com.dtflys.easyel.ast.ASTNegativeExpression;
import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.ast.ASTNotExpression;
import com.dtflys.easyel.ast.ASTRange;
import com.dtflys.easyel.ast.ASTTernaryExpression;
import com.dtflys.easyel.ast.ASTTimeDuration;
import com.dtflys.easyel.ast.ASTType;
import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TestEasyElAST extends CompilerUnit {

    @Test
    public void testExpr() {
        String source = "203 + 2.02 + 'ok'";
        ASTNode node = compile(source);
        assertNotNull(node);
    }

    @Test
    public void testType() {
        assertEquals("Undefined", ASTType.UNDEFINED.toString());

        ASTType type = ASTType.fromJavaClass(int.class);
        assertEquals("java.lang.Integer", type.toString());

        type = ASTType.fromJavaClass(long.class);
        assertEquals("java.lang.Long", type.toString());

        type = ASTType.fromJavaClass(float.class);
        assertEquals("java.lang.Float", type.toString());

        type = ASTType.fromJavaClass(double.class);
        assertEquals("java.lang.Double", type.toString());

        type = ASTType.fromJavaClass(Object.class);
        assertEquals("java.lang.Object", type.toString());

        type = ASTType.fromJavaClass(TestEasyElAST.class);
        assertEquals("com.dtflys.easyel.TestEasyElAST", type.toString());
    }

    @Test
    public void testNegativeExpr() {
        String source = "-23";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTNegativeExpression);
        assertEquals("-23", node.toString());
//        assertEquals(ASTType.INTEGER, ((ASTNegativeExpression) node).getType());

        source = "-2.12";
        node = compile(source);
        assertTrue(node instanceof ASTNegativeExpression);
        assertEquals(ASTType.NUMERIC, ((ASTNegativeExpression) node).getType());
        assertEquals("-2.12", node.toString());


        source = "-x";
        node = compile(source);
        assertTrue(node instanceof ASTNegativeExpression);
        assertEquals(ASTType.UNDEFINED, ((ASTNegativeExpression) node).getType());
        assertEquals("-<x>", node.toString());

        source = "+x";
        node = compile(source);
        assertTrue(node instanceof ASTIdentifier);
        assertEquals("<x>", node.toString());
    }

    @Test
    public void testNotExpr() {
        String source = "!(a > b)";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTNotExpression);
        assertEquals("not (<a> > <b>)", node.toString());
        ASTNotExpression notExpression = (ASTNotExpression) node;
        assertEquals("(<a> > <b>)", notExpression.getExpression().getText());

        source = "!x";
        node = compile(source);
        assertTrue(node instanceof ASTNotExpression);
        assertEquals("not <x>", node.toString());
        notExpression = (ASTNotExpression) node;
        assertEquals("x", notExpression.getExpression().getText());

        source = "not(a > b)";
        node = compile(source);
        assertTrue(node instanceof ASTNotExpression);
        assertEquals("not (<a> > <b>)", node.toString());
        notExpression = (ASTNotExpression) node;
        assertEquals("(<a> > <b>)", notExpression.getExpression().getText());

        source = "not isSuccess";
        node = compile(source);
        assertTrue(node instanceof ASTNotExpression);
        assertEquals("not <isSuccess>", node.toString());
        notExpression = (ASTNotExpression) node;
        assertEquals("isSuccess", notExpression.getExpression().getText());
    }

    @Test
    public void testRangeExpr() {
        String source = "[1..2]";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTRange);
        ASTRange range = (ASTRange) node;
        assertEquals("1", range.getFrom().toString());
        assertEquals("2", range.getTo().toString());
        assertEquals(true, range.isIncludeFrom());
        assertEquals(true, range.isIncludeTo());
        assertEquals("[1 .. 2]", node.toString());


        source = "(0..5]";
        node = compile(source);
        assertTrue(node instanceof ASTRange);
        range = (ASTRange) node;
        assertEquals("0", range.getFrom().toString());
        assertEquals("5", range.getTo().toString());
        assertEquals(false, range.isIncludeFrom());
        assertEquals(true, range.isIncludeTo());
        assertEquals("(0 .. 5]", node.toString());


        source = "[0..5)";
        node = compile(source);
        assertTrue(node instanceof ASTRange);
        range = (ASTRange) node;
        assertEquals("0", range.getFrom().toString());
        assertEquals("5", range.getTo().toString());
        assertEquals(true, range.isIncludeFrom());
        assertEquals(false, range.isIncludeTo());
        assertEquals("[0 .. 5)", node.toString());


        source = "(0..5)";
        node = compile(source);
        assertTrue(node instanceof ASTRange);
        range = (ASTRange) node;
        assertEquals("0", range.getFrom().toString());
        assertEquals("5", range.getTo().toString());
        assertEquals(false, range.isIncludeFrom());
        assertEquals(false, range.isIncludeTo());
        assertEquals("(0 .. 5)", node.toString());
    }


    @Test
    public void testNewInstance() {
        ASTNode node = compile("new java.lang.Integer('2')");
        assertNotNull(node);
        assertEquals("new java.lang.Integer(2)", node.toString());
    }

    @Test
    public void testInvoke() {
        ASTNode node = compile("foo.func(bar)");
        assertTrue(node instanceof ASTInvokeExpression);
        assertEquals("<foo>.func(<bar>)", node.toString());

        node = compile("java.lang.Math.max(1, 2)");
        assertTrue(node instanceof ASTInvokeExpression);
        assertEquals("java.lang.Math.max(1, 2)", node.toString());
    }

    @Test
    public void testDateExpr() {
        String source = "2018-01-11";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11", node.toString());

        source = "2018-01-11 08:01";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 08:01", node.toString());

        source = "2018-01-11 03:12:40";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 03:12:40", node.toString());

        source = "2018-01-11 03:12:40:05";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 03:12:40:05", node.toString());

        source = "2018.01.11 08:01";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 08:01", node.toString());

        source = "2018/01/11 03:12";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 03:12", node.toString());

        source = "2018\\01\\11 08:01";
        node = compile(source);
        assertTrue(node instanceof ASTDate);
        assertEquals("2018-01-11 08:01", node.toString());
    }


    @Test
    public void testTimeDuration() {
        ASTNode node = compile("10:00");
        assertTrue(node instanceof ASTTimeDuration);
        assertEquals("10:00", node.toString());

        node = compile("10:14:01");
        assertTrue(node instanceof ASTTimeDuration);
        assertEquals("10:14:01", node.toString());

        node = compile("10:14:01:55");
        assertTrue(node instanceof ASTTimeDuration);
        assertEquals("10:14:01:55", node.toString());
    }

    @Test
    public void testTernary() {
        ASTNode node = compile("true ? 1 : 0");
        assertTrue(node instanceof ASTTernaryExpression);
        assertEquals("true ? 1 : 0", node.toString());

        node = compile("1 < 3 ? 1 : 0");
        assertTrue(node instanceof ASTTernaryExpression);
        assertEquals("(1 < 3) ? 1 : 0", node.toString());

        node = compile("1 < 3 ? 1");
        assertTrue(node instanceof ASTTernaryExpression);
        assertEquals("(1 < 3) ? 1", node.toString());

        node = compile("1 < 3 ?: 1");
        assertTrue(node instanceof ASTTernaryExpression);
        assertEquals("(1 < 3) ?: 1", node.toString());
    }

    @Test
    public void testListExpr() {
        String source = "[1, 2, 3, 4, a, b, c]";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTList);
        assertEquals("[1, 2, 3, 4, <a>, <b>, <c>]", node.toString());

        source = "[a, 1 * (b + 3), [], ]";
        node = compile(source);
        assertTrue(node instanceof ASTList);
        assertEquals("[<a>, (1 * (<b> + 3)), []]", node.toString());
    }

    @Test
    public void testListGenExpr() {
        ASTNode node = compile("[x for x in [1, 2, 3]]");
        assertTrue(node instanceof ASTListGenerator);
        assertEquals("[<x> for x in [1, 2, 3]]", node.toString());

        node = compile("[a + b for a, b in [[1, 2], [3, 4]]]");
        assertTrue(node instanceof ASTListGenerator);
        assertEquals("[(<a> + <b>) for a, b in [[1, 2], [3, 4]]]", node.toString());
    }

    @Test
    public void testMapExpr() {
        String source = "{a: 1, b: 2}";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTMap);
        assertEquals("{a: 1, b: 2}", node.toString());

        source = "{a: 1 + 2, b: x + y, }";
        node = compile(source);
        assertTrue(node instanceof ASTMap);
        assertEquals("{a: (1 + 2), b: (<x> + <y>)}", node.toString());

        source = "{0: 1 + 2, 1: x + y, 'ok': 'yes'}";
        node = compile(source);
        assertTrue(node instanceof ASTMap);
        assertEquals("{0: (1 + 2), 1: (<x> + <y>), ok: yes}", node.toString());


        source = "{null: null, true: true, false: false}";
        node = compile(source);
        assertTrue(node instanceof ASTMap);
        assertEquals("{null: <null>, true: true, false: false}", node.toString());
    }


    @Test
    public void testID() {
        String source = "x + y";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTBinaryExpression);
    }


    @Test
    public void testIn() {
        String source = "x in [a, b, c, d]";
        ASTNode node = compile(source);
        assertTrue(node instanceof ASTBinaryExpression);
        assertEquals(ASTType.BOOLEAN, ((ASTBinaryExpression) node).getType());

        source = "x !in [1, 2, 3]";
        node = compile(source);
        assertTrue(node instanceof ASTBinaryExpression);
        assertEquals(ASTType.BOOLEAN, ((ASTBinaryExpression) node).getType());

        source = "x not in [1, 2, 3]";
        node = compile(source);
        assertTrue(node instanceof ASTBinaryExpression);
    }

    @Test
    public void testIndex() {
        ASTNode node = compile("list[0]");
        assertTrue(node instanceof ASTIndexExpression);
        assertEquals("<list>[0]", node.toString());

        node = compile("list[a + b]");
        assertTrue(node instanceof ASTIndexExpression);
        assertEquals("<list>[(<a> + <b>)]", node.toString());


        node = compile("map['a']");
        assertTrue(node instanceof ASTIndexExpression);
        assertEquals("<map>[a]", node.toString());

        node = compile("map['a' + 'b']");
        assertTrue(node instanceof ASTIndexExpression);
        assertEquals("<map>[(a + b)]", node.toString());
    }

    @Test
    public void testAccessExpression() {
        String source = "a.b.c";
        ASTNode node = compile(source);
        System.out.println(node);
    }



}
