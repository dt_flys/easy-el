package com.dtflys.easyel;

import com.dtflys.easyel.ast.ASTNode;
import com.dtflys.easyel.compile.EasyElCompiledResult;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public abstract class CompilerUnit {

    protected ASTNode compile(String source) {
        EasyElCompiledResult result = EasyEl.compile(source);
        return result.getAstNode();
    }

}
