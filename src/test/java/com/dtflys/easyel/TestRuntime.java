package com.dtflys.easyel;

import com.dtflys.easyel.runtime.EasyElDate;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TestRuntime {

    @Test
    public void testDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 2 - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 3);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.setTimeZone(TimeZone.getDefault());
        Date date = calendar.getTime();
        EasyElDate easyElDate = new EasyElDate(date);
        assertEquals(date.getTime(), easyElDate.getDate().getTime());
        assertEquals("2018-02-03 +0800(CST)", easyElDate.toString());
        assertEquals(2018, easyElDate.getYear());
        assertEquals(2, easyElDate.getMonth());
        assertEquals(3, easyElDate.getDay());


        calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2018);
        calendar.set(Calendar.MONTH, 2 - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 3);
        calendar.set(Calendar.HOUR_OF_DAY, 14);
        calendar.set(Calendar.MINUTE, 5);
        calendar.set(Calendar.SECOND, 0);
        date = calendar.getTime();
        easyElDate = new EasyElDate(date);
        assertEquals(date.getTime(), easyElDate.getDate().getTime());
        assertEquals(14, easyElDate.getHour());
        assertEquals(5, easyElDate.getMinute());
    }
}
