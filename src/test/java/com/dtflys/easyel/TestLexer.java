package com.dtflys.easyel;

import com.dtflys.easyel.compile.EasyElSource;
import com.dtflys.easyel.parser.ELexer;
import com.dtflys.easyel.parser.EToken;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestLexer {
    
    @Test
    public void testWS() throws IOException {
        String text = "    \t";
        EasyElSource source = new EasyElSource(text);
        ELexer lexer = new ELexer(source);
        EToken token = lexer.nextToken(false);
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.WS);
        assertThat(token.getSymbolicName()).isEqualTo("WS");
        assertThat(token.getLine()).isEqualTo(1);
        assertThat(token.getText()).isEqualTo(text);
        assertThat(token.getStartColumn()).isEqualTo(1);
        assertThat(token.getEndColumn()).isEqualTo(text.length());
        assertThat(token.isSkip()).isTrue();
    }


    @Test
    public void testNL() throws IOException {
        String text = "\n ";
        EasyElSource source = new EasyElSource(text);
        ELexer lexer = new ELexer(source);
        EToken token = lexer.nextToken(false);
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.NL);
        assertThat(token.getSymbolicName()).isEqualTo("NL");
        assertThat(token.getText()).isEqualTo("\n");

        token = lexer.nextToken(false);
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.WS);

        token = lexer.nextToken();
        assertThat(token.getType()).isEqualTo(EToken.EOF);
    }
    
    @Test
    public void testInteger() throws IOException {
        String text = "123";
        EasyElSource source = new EasyElSource(text);
        ELexer lexer = new ELexer(source);
        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER); 
        assertThat(token.getSymbolicName()).isEqualTo("INTEGER");
        assertThat(token.getText()).isEqualTo("123");
        assertThat(token.getStartColumn()).isEqualTo(1);
        assertThat(token.getEndColumn()).isEqualTo(text.length());
    }

    @Test
    public void testHex() throws IOException {
        String text = "0x31ab";
        EasyElSource source = new EasyElSource(text);
        ELexer lexer = new ELexer(source);
        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.HEX);
        assertThat(token.getSymbolicName()).isEqualTo("HEX");
        assertThat(token.getText()).isEqualTo("0x31ab");
        assertThat(token.getStartColumn()).isEqualTo(1);
        assertThat(token.getEndColumn()).isEqualTo(text.length());
    }

    @Test
    public void testIdentifier() throws IOException {
        EasyElSource source = new EasyElSource("abc a_12 _232 _a3");
        ELexer lexer = new ELexer(source);
        
        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("abc");
        assertThat(token.getStartColumn()).isEqualTo(1);
        assertThat(token.getEndColumn()).isEqualTo("abc".length());
        
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a_12");
        assertThat(token.getStartColumn()).isEqualTo(5);
        assertThat(token.getEndColumn()).isEqualTo(8);

        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("_232");

        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("_a3");
    }


    @Test
    public void testSimpleExprTokens() throws IOException {
        String text = "a = (0 + 32 - b) * 2.23 / 0.8";
        EasyElSource source = new EasyElSource(text);
        ELexer lexer = new ELexer(source);
        
        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getSymbolicName()).isEqualTo("IDENTIFIER");
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.ASSIGN);
        assertThat(token.getSymbolicName()).isEqualTo("ASSIGN");
        assertThat(token.getText()).isEqualTo("=");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.LPAREN);
        assertThat(token.getSymbolicName()).isEqualTo("LPAREN");
        assertThat(token.getText()).isEqualTo("(");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER);
        assertThat(token.getSymbolicName()).isEqualTo("INTEGER");
        assertThat(token.getText()).isEqualTo("0");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.PLUS);
        assertThat(token.getSymbolicName()).isEqualTo("PLUS");
        assertThat(token.getText()).isEqualTo("+");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER);
        assertThat(token.getSymbolicName()).isEqualTo("INTEGER");
        assertThat(token.getText()).isEqualTo("32");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.MINUS);
        assertThat(token.getSymbolicName()).isEqualTo("MINUS");
        assertThat(token.getText()).isEqualTo("-");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getSymbolicName()).isEqualTo("IDENTIFIER");
        assertThat(token.getText()).isEqualTo("b");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.RPAREN);
        assertThat(token.getSymbolicName()).isEqualTo("RPAREN");
        assertThat(token.getText()).isEqualTo(")");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.MUL);
        assertThat(token.getSymbolicName()).isEqualTo("MUL");
        assertThat(token.getText()).isEqualTo("*");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DECIMAL);
        assertThat(token.getSymbolicName()).isEqualTo("DECIMAL");
        assertThat(token.getText()).isEqualTo("2.23");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DIV);
        assertThat(token.getSymbolicName()).isEqualTo("DIV");
        assertThat(token.getText()).isEqualTo("/");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DECIMAL);
        assertThat(token.getSymbolicName()).isEqualTo("DECIMAL");
        assertThat(token.getText()).isEqualTo("0.8");
    }

    @Test
    public void testPower() throws IOException {
        EasyElSource source = new EasyElSource("a ** 2");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DOUBLE_STAR);
        assertThat(token.getSymbolicName()).isEqualTo("DOUBLE_STAR");
        assertThat(token.getText()).isEqualTo("**");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER);
        assertThat(token.getText()).isEqualTo("2");
    }



    @Test
    public void testNotEq() throws IOException {
        EasyElSource source = new EasyElSource("a != b");
        ELexer lexer = new ELexer(source);
        
        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.NOT_EQ);
        assertThat(token.getSymbolicName()).isEqualTo("NOT_EQ");
        assertThat(token.getText()).isEqualTo("!=");
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("b");
    }


    @Test
    public void testNotRegexMatch() throws IOException {
        EasyElSource source = new EasyElSource("a !=~ /[a-z0-9]*/");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.NOT_REGEX_MATCH);
        assertThat(token.getSymbolicName()).isEqualTo("NOT_REGEX_MATCH");
        assertThat(token.getText()).isEqualTo("!=~");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.REGEX_PATTERN);
        assertThat(token.getText()).isEqualTo("/[a-z0-9]*/");
        assertThat(token.getStartColumn()).isEqualTo(7);
        assertThat(token.getEndColumn()).isEqualTo(17);
    }


    @Test
    public void testNotIn() throws IOException {
        EasyElSource source = new EasyElSource("a !in b");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.NOT_IN);
        assertThat(token.getSymbolicName()).isEqualTo("NOT_IN");
        assertThat(token.getText()).isEqualTo("!in");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("b");
    }


    @Test
    public void testNot() throws IOException {
        EasyElSource source = new EasyElSource("a = !b");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.ASSIGN);
        assertThat(token.getSymbolicName()).isEqualTo("ASSIGN");
        assertThat(token.getText()).isEqualTo("=");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.NOT);
        assertThat(token.getText()).isEqualTo("!");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("b");
    }


    @Test
    public void testIncrementDecrement() throws IOException {
        EasyElSource source = new EasyElSource("a++ b--");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.PLUS_PLUS);
        assertThat(token.getSymbolicName()).isEqualTo("PLUS_PLUS");
        assertThat(token.getText()).isEqualTo("++");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("b");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.MINUS_MINUS);
        assertThat(token.getSymbolicName()).isEqualTo("MINUS_MINUS");
        assertThat(token.getText()).isEqualTo("--");
    }

    @Test
    public void testDoubleQuoteString() throws IOException {
        EasyElSource source = new EasyElSource("a = \"abc93 23jl \\n sf'ad'#7921\"");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.ASSIGN);
        assertThat(token.getText()).isEqualTo("=");
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DOUBLE_QUOTE_STRING);
        assertThat(token.getSymbolicName()).isEqualTo("DOUBLE_QUOTE_STRING");
        assertThat(token.getText()).isEqualTo("\"abc93 23jl \\n sf'ad'#7921\"");
    }

    @Test
    public void testSingleQuoteString() throws IOException {
        EasyElSource source = new EasyElSource("a = 'abc93 23jl \\n sf\"ad\"#7921'");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.ASSIGN);
        assertThat(token.getText()).isEqualTo("=");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.SINGLE_QUOTE_STRING);
        assertThat(token.getSymbolicName()).isEqualTo("SINGLE_QUOTE_STRING");
        assertThat(token.getText()).isEqualTo("'abc93 23jl \\n sf\"ad\"#7921'");
    }

    @Test
    public void testDot() throws IOException {
        EasyElSource source = new EasyElSource("a.b.c()");
        ELexer lexer = new ELexer(source);

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DOT);
        assertThat(token.getText()).isEqualTo(".");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("b");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DOT);
        assertThat(token.getText()).isEqualTo(".");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("c");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.LPAREN);
        assertThat(token.getText()).isEqualTo("(");

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.RPAREN);
        assertThat(token.getText()).isEqualTo(")");
    }

    @Test
    public void testDoubleDot() throws IOException {
        EasyElSource source = new EasyElSource("a[0 .. 9]");
        ELexer lexer = new ELexer(source);
        List<EToken> tokens = new ArrayList<>();

        EToken token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.IDENTIFIER);
        assertThat(token.getText()).isEqualTo("a");
        tokens.add(token);

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.LBRACK);
        assertThat(token.getText()).isEqualTo("[");
        tokens.add(token);
        
        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER);
        assertThat(token.getText()).isEqualTo("0");
        tokens.add(token);

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.DOUBLE_DOT);
        assertThat(token.getText()).isEqualTo("..");
        tokens.add(token);

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.INTEGER);
        assertThat(token.getText()).isEqualTo("9");
        tokens.add(token);

        token = lexer.nextToken();
        assertThat(token).isNotNull();
        assertThat(token.getType()).isEqualTo(EToken.RBRACK);
        assertThat(token.getText()).isEqualTo("]");
        tokens.add(token);

        token = lexer.nextToken();
        tokens.add(token);

        System.out.println(tokens);
    }

}
