package com.dtflys.easyel;

import com.dtflys.easyel.exception.EasyElEvalException;
import com.dtflys.easyel.exception.EasyElNullException;
import com.dtflys.easyel.module.User;
import com.dtflys.easyel.runtime.EasyElDate;
import com.dtflys.easyel.runtime.EasyElTimeDuration;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since v1.0.0
 */
public class TestEasyElEvaluate extends CompilerUnit {



    private Object eval(String source, Map<String, Object> env) {
        EasyElContext context = new EasyElContext();
        context.setEnv(env);
//        context.getCompileConfiguration().setSourceName("[SCRIPT in TestEasyElEvaluate.java]");
        return EasyEl.eval(source, context);
    }

    public Object eval(String source) {
        return eval(source, new HashMap<String, Object>());
    }


    @Test
    public void testError() {
        String source = "'a' == a";
        Map<String, Object> env = new HashMap<>();
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        Throwable th = null;
        try {
            eval(source, env);
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

        th = null;
        try {
            eval("null.a");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);
        assertTrue(th instanceof EasyElNullException);


        th = null;
        try {
            eval("1.x");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);
        assertTrue(th instanceof EasyElEvalException);

    }


    @Test
    public void testInvokeStatic() {
        String source = "Math.max(1, Math.min(Math.abs(-3.2), 5.1))";
        Object ret = eval(source);
        assertNotNull(ret);
        assertEquals(new Double(3.2), ret);
        System.out.println(ret + "\n");

        ret = eval("com.dtflys.easyel.TestEasyElEvaluate.A.staticMethod()");
        assertEquals("success", ret);
    }

    @Test
    public void testInvokeStaticMethod() {
        Object ret = eval("java.lang.Math.max(1, 2)");
        assertEquals(new Integer(2), ret);
    }

    @Test
    public void testStaticProperty() {
        Object ret = eval("com.dtflys.easyel.TestEasyElEvaluate.A.CONST");
        assertEquals("const", ret);

        ret = eval("com.dtflys.easyel.TestEasyElEvaluate.A.class.name");
        assertEquals("com.dtflys.easyel.TestEasyElEvaluate$A", ret);
    }

    public static class A {

        public final static String CONST = "const";

        public static String staticMethod() {
            return "success";
        }

        public static class InnerA {}

        public String testVar(String a, int... vars) {
            StringBuilder builder = new StringBuilder(a);
            for (int i = 0; i < vars.length; i++) {
                builder.append('-');
                builder.append(vars[i]);
            }
            return builder.toString();
        }

        public String varA(String a, int... vars) {
            return testVar(a, vars);
        }

        public String varA(String a, String b, int... vars) {
            return varA(a + ":" + b, vars);
        }

        public String varA(String a, String b, String c, int... vars) {
            return varA(a + ":" + b, c, vars);
        }

    }
    public static class B extends A {}
    public static class C extends B {}
    public static class D extends C {}

    public static class T {
        public String choose(A obj) {return "A";}
        public String choose(B obj) {return "B";}
        public String choose(C obj) {return "C";}
        public String choose2(A objA, B objB) {return "A and B";}
        public String choose2(B objB, A objA) {return "B and A";}
        public String choose2(C objB, B objA) {return "C and B";}

    }

    @Test
    public void testInvokeMethod() {
        String source = "t.choose(v)";
        Map<String, Object> env = new HashMap<>();
        env.put("t", new T());
        env.put("v", new A());
        Object ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("A", ret);
        System.out.println(ret);

        source = "t.choose(v)";
        env.put("t", new T());
        env.put("v", new B());
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("B", ret);
        System.out.println(ret);

        source = "t.choose(v)";
        env.put("t", new T());
        env.put("v", new D());
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("C", ret);
        System.out.println(ret);

        source = "t.choose(v)";
        env.put("t", new T());
        env.put("v", new C());
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("C", ret);
        System.out.println(ret);

        source = "t.choose2(v1, v2)";
        env.put("t", new T());
        env.put("v1", new A());
        env.put("v2", new B());
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("A and B", ret);
        System.out.println(ret);

        source = "t.choose2(v1, v2)";
        env.put("t", new T());
        env.put("v1", new B());
        env.put("v2", new A());
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("B and A", ret);
        System.out.println(ret);

    }


    @Test
    public void testInvokeVarArgs() {
        EasyElContext context = new EasyElContext();
        context.setEnv("a", new A());
        String ret = EasyEl.evalAsString("a.testVar('haha', 2, 4, 1, 5)", context);
        assertEquals("haha-2-4-1-5", ret);
    }


    @Test
    public void testInvokeOverloadVarArgs() {
        EasyElContext context = new EasyElContext();
        context.setEnv("a", new A());
        String ret = EasyEl.evalAsString("a.varA('haha', 2, 4, 1, 5)", context);
        assertEquals("haha-2-4-1-5", ret);

        ret = EasyEl.evalAsString("a.varA('haha', 'xx', 2, 4, 1, 5)", context);
        assertEquals("haha:xx-2-4-1-5", ret);

        ret = EasyEl.evalAsString("a.varA('haha', 'xx', 'yy', 2, 4, 1, 5)", context);
        assertEquals("haha:xx:yy-2-4-1-5", ret);
    }


    @Test
    public void testInvokeOverwriteMethod() {
        String source = "t.choose(v)";
        Map<String, Object> env = new HashMap<>();
        env.put("t", new T());
        env.put("v", new D());
        Object ret = eval(source, env);
        assertNotNull(ret);
        assertEquals("C", ret);
        System.out.println(ret);
    }


    @Test
    public void testAmbiguousMethods() throws Throwable {

        String source = "t.choose2(v1, v2)";
        Map<String, Object> env = new HashMap<>();
        env.put("t", new T());
        env.put("v1", new B());
        env.put("v2", new B());
        Throwable t = null;
        try {
            eval(source, env);
        } catch (EasyElEvalException e) {
            t = e;
        }
        assertNotNull(t);
//        new RuntimeException(t).printStackTrace();
//        throw t;
    }


    @Test
    public void testCompare() {
        String source = "10 < 10.1";
        Object ret = eval(source);
        assertNotNull(ret);
        assertEquals(true, ret);
    }


    @Test
    public void testBinaryExpression() {
        Throwable th = null;

        String source = "1 + 2 * 4.2";
        Object ret = eval(source);
        assertNotNull(ret);
        assertEquals(new BigDecimal("9.4"), ret);
        System.out.println(ret);

        source = "a + b";
        Map<String, Object> env = new HashMap<>();
        env.put("a", new Integer(2));
        env.put("b", new Integer(5));
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals(new Integer(7), ret);

        try {
            env.put("a", new A());
            env.put("b", new A());
            eval("a + b");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);


        source = "a ** b";
        env.put("a", new Integer(5));
        env.put("b", new Integer(2));
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals(new BigInteger("25"), ret);
        System.out.println(ret);

        th = null;
        try {
            eval("2 ** 2.4");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

        source = "a % b";
        env.put("a", new Integer(5));
        env.put("b", new Integer(2));
        ret = eval(source, env);
        assertNotNull(ret);
        assertEquals(new Integer(1), ret);
        System.out.println(ret);


        source = "a + b == 10";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);


        source = "a + b != 4";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);


        source = "a + b != 10";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertFalse((Boolean) ret);
        System.out.println(ret);


        source = "a + b > 5";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);


        source = "a + b <= 10 and a <= 5";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);

        source = "a + b >= 10 and b >= 5";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);

        th = null;
        try {
            source = "a >= b";
            env.put("a", new A());
            env.put("b", new A());
            ret = eval(source, env);
            assertNotNull(ret);
            assertTrue((Boolean) ret);
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

        source = "a + b < c and b * 2 == c + 4";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        env.put("c", new Integer(12));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);


        source = "a + b < c and b * 2 == c + 4";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        env.put("c", new Integer(12));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);

        source = "a + b < c and b * 2 == c + 4 and (a > 1 or b < 2)";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        env.put("c", new Integer(12));
        ret = eval(source, env);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);


        source = "a + b < c and b < 2";
        env.put("a", new Integer(2));
        env.put("b", new Integer(8));
        env.put("c", new Integer(12));
        ret = eval(source, env);
        assertNotNull(ret);
        assertFalse((Boolean) ret);
        System.out.println(ret);
    }

    @Test
    public void testMap() {
        EasyElContext context = new EasyElContext();
        Map map = EasyEl.evalAsMap("{a: 1, b: 2}", context);
        assertNotNull(map);
        assertEquals(new Integer(1), map.get("a"));
        assertEquals(new Integer(2), map.get("b"));
    }

    private void assertList(List except, List actual) {
        assertEquals(except.size(), actual.size());
        for (int i = 0; i < except.size(); i++) {
            Object ex = except.get(i);
            Object ac = actual.get(i);
            assertEquals(ex, ac);
        }
    }

    @Test
    public void testList() {
        String source = "[1, 2, 3, 4]";
        Object ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof List);
        System.out.println(ret);

        source = "[1, 2 / 2 + 1, 3 * 0.5, 4 + 3 * 2]";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof List);
        System.out.println(ret);
    }

    @Test
    public void testListGenerator() {
        Object result = eval("[x * 2 for x in [1, 2, 3]]");
        assertList(Arrays.asList(2, 4, 6), (List) result);
        System.out.println(result);

        Map<String, Object> env = new HashMap<>();
        env.put("lst", Arrays.asList(2, 4, 8, 9));
        result = eval("[x + 1 for x in lst]", env);
        assertList(Arrays.asList(3, 5, 9, 10), (List) result);
        System.out.println(result);

        result = eval("[a + b for a, b in [[1, 2], [3, 4]]]");
        assertList(Arrays.asList(3, 7), (List) result);
        System.out.println(result);

        result = eval("[v for v in {a: 1, b: 2}]");
        assertTrue(result instanceof List);
        assertEquals(2, ((List) result).size());
        assertTrue(((List) result).contains(1));
        assertTrue(((List) result).contains(2));
        System.out.println(result);

        result = eval("[k for k, v in {a: 1, b: 2}]");
        assertTrue(result instanceof List);
        assertEquals(2, ((List) result).size());
        assertTrue(((List) result).contains("a"));
        assertTrue(((List) result).contains("b"));
        System.out.println(result);

        result = eval("[k + v for k, v in {a: 1, b: 2}]");
        assertTrue(result instanceof List);
        assertEquals(2, ((List) result).size());
        assertTrue(((List) result).contains("a1"));
        assertTrue(((List) result).contains("b2"));
        System.out.println(result);

        result = eval("[x for x in 'abcd']");
        assertList(Arrays.asList('a', 'b', 'c', 'd'), (List) result);
        System.out.println(result);

        result = eval("[x for x in [1..10]]");
        assertList(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), (List) result);
        System.out.println(result);

        result = eval("[x for x in [1..10].step(2)]");
        assertList(Arrays.asList(1, 3, 5, 7, 9), (List) result);
        System.out.println(result);

        result = eval("[x for x in [2018-01-02 .. 2020-01-01]]");
        System.out.println(result);


        result = eval("[x for x in ['a'..'d']]");
        assertList(Arrays.asList("a", "b", "c", "d"), (List) result);
        System.out.println(result);

        Throwable th = null;
        try {
            eval("[a for a, b, c in [1, 2, 3]]");
        } catch (Throwable t) {
            th = t;
        }
        assertNotNull(th);
    }


    @Test
    public void testNumber() {
        Object ret = eval("10");
        assertEquals(new Integer(10), ret);

        ret = eval("-1");
        assertEquals(new Integer(-1), ret);

        ret = eval("0");
        assertEquals(new Integer(0), ret);

        ret = eval((Long.MAX_VALUE - 100) + "");
        assertEquals(Long.MAX_VALUE - 100, ret);

        ret = eval("10L");
        assertEquals(new Long(10), ret);

        ret = eval("0L");
        assertEquals(new Long(0), ret);

        ret = eval("10.0");
        assertEquals(new BigDecimal("10.0"), ret);

        ret = eval("10.0f");
        assertEquals(new Float("10.0"), ret);

        ret = eval("10.0D");
        assertEquals(new Double("10.0"), ret);

        ret = eval("10.0G");
        assertEquals(new BigDecimal("10.0"), ret);

        ret = eval("10 == 10L");
        assertTrue((Boolean) ret);

        ret = eval("10 == 10.0f");
        assertTrue((Boolean) ret);

        ret = eval("10.0F == 10.0d");
        assertTrue((Boolean) ret);

    }

    @Test
    public void testNot() {
        Object ret = eval("!(1 > 0)");
        assertFalse((Boolean) ret);

        ret = eval("not(1 > 0)");
        assertFalse((Boolean) ret);

        ret = eval("not true");
        assertFalse((Boolean) ret);

        ret = eval("not false");
        assertTrue((Boolean) ret);
    }

    @Test
    public void testDate() {
        String source = "2018-01-01";
        Object ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElDate);
        System.out.println(ret);

        source = "2018-02-02 10:05";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElDate);
        System.out.println(ret);

        source = "2018.11.02 10:05:02";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElDate);
        System.out.println(ret);

        source = "2018/07/02 10:05:02:33";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElDate);
        System.out.println(ret);
    }

    @Test
    public void testTimeDuration() {
        String source = "10:30";
        Object ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElTimeDuration);
        System.out.println(ret);

        source = "10:02:32";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElTimeDuration);
        System.out.println(ret);
    }

    @Test
    public void testDateAndTime() {
        String source = "2018-02-02 10:05 - 00:05";
        Object ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElDate);
        assertEquals(new Integer(10), new Integer(((EasyElDate) ret).getHour()));
        assertEquals(new Integer(0), new Integer(((EasyElDate) ret).getMinute()));
        System.out.println(ret);


        source = "2018-02-02 10:05 - 2018-02-02 00:00";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue(ret instanceof EasyElTimeDuration);
        System.out.println(ret);


        source = "2018/02/02 10:05 - 2018/02/02 00:06 < 10:00";
        ret = eval(source);
        assertNotNull(ret);
        assertTrue((Boolean) ret);
        System.out.println(ret);

    }


    @Test
    public void testRange() {
        String source = "[1 .. 10]";
        Object ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = "(1 .. 10]";
        ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = "[1 .. 10)";
        ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = "(1 .. 10)";
        ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = "( .. 10)";
        ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = "(10 .. )";
        ret = eval(source);
        assertNotNull(ret);
        System.out.println(ret);

        source = " 1 in (.. 10)";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = " 11 in (.. 10)";
        ret = eval(source);
        assertFalse((Boolean) ret);

        source = " 999 in (10 .. )";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = " 10 in (10 .. )";
        ret = eval(source);
        assertFalse((Boolean) ret);

    }

    @Test
    public void testNewInstance() {
        Object ret = eval("new java.lang.Integer('2')");
        assertEquals(new Integer(2), ret);

        ret = eval("new Integer('2') + 2");
        assertEquals(new Integer(4), ret);

        Throwable th = null;
        try {
            eval("new Xxx('2')");
        } catch (Throwable t) {
            th = t;
        }
        assertNotNull(th);

        EasyElContext context = new EasyElContext();
        context.importClass("A", A.class);
        ret = EasyEl.eval("new A()", context);
        assertNotNull(ret);

        th = null;
        context = new EasyElContext();
        context.importClass("A", A.class);
        try {
            EasyEl.eval("new A(333)", context);
        } catch (Throwable t) {
            th = t;
        }
        assertNotNull(th);
        assertTrue(th.toString().contains("[EASYEL] error[runtime]"));
        System.out.println(th.toString());


        context = new EasyElContext();
        ret = EasyEl.eval("new com.dtflys.easyel.module.User() {name: 'haha', password: '1234321'}", context);
        assertNotNull(ret);
        assertEquals("haha", ((User) ret).getName());
        assertEquals("1234321", ((User) ret).getPassword());

        context = new EasyElContext();
        context.importClass("User", User.class);
        ret = EasyEl.eval("new User() {name: 'haha'}.say", context);
        assertNotNull(ret);
        assertEquals("My Name is haha", ret);
    }

    @Test
    public void testInnerClass() {
        EasyElContext context = new EasyElContext();
        context.importClass("A", A.class);
        Object ret = EasyEl.eval("new A.InnerA()", context);
        assertNotNull(ret);

        ret = EasyEl.eval("new com.dtflys.easyel.TestEasyElEvaluate.A()", context);
        assertNotNull(ret);

        ret = EasyEl.eval("new com.dtflys.easyel.TestEasyElEvaluate.A.InnerA().class.name", context);
        assertEquals("com.dtflys.easyel.TestEasyElEvaluate$A$InnerA", ret);
    }

    @Test
    public void testIn() {
        String source = "2 in [1, 2, 3]";
        Object ret = eval(source);
        assertTrue((Boolean) ret);

        source = "2 in [1..3]";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "2 in (1..2]";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "2 in (2..6]";
        ret = eval(source);
        assertFalse((Boolean) ret);

        source = "2018/01/01 10:00 in (2018/01/01 08:00 .. 2018/01/01 11:00)";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "'a' in ['a', 'b']";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "'a' !in ['A', 'B']";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "'ok' in 'I am ok'";
        ret = eval(source);
        assertTrue((Boolean) ret);

        source = "'ok' !in 'I am ok'";
        ret = eval(source);
        assertFalse((Boolean) ret);

        source = "'ok' not in 'I am ok'";
        ret = eval(source);
        assertFalse((Boolean) ret);
    }

    @Test
    public void testRegexMatch() {
        Object ret = eval("'123abc' =~ '[\\d]+[a-z]+'");
        assertTrue((Boolean) ret);

        ret = eval("'123abc' !=~ '[\\d]+[a-z]+'");
        assertFalse((Boolean) ret);
    }

    @Test
    public void testElvis() {
        Object ret = eval("false ?: 'b'");
        assertEquals("b", ret);

        ret = eval("true ?: 'b'");
        assertTrue((Boolean) ret);

        ret = eval("'a' ?: 'b'");
        assertEquals("a", ret);

        ret = eval("false ?: 'b'");
        assertEquals("b", ret);

        ret = eval("null ?: 'b'");
        assertEquals("b", ret);
    }

    @Test
    public void testTypeCheck() {
        boolean fail = false;
        try {
            eval("123 == '123'");
        } catch (Throwable th) {
            th.printStackTrace();
            fail = true;
        }
        assertTrue(fail);
    }

    @Test
    public void testTernaryExpression() {
        Object ret = eval("1 > 0 ? 'a' : 'b'");
        assertEquals("a", ret);

        ret = eval("1 > 2 ? 'a' : 'b'");
        assertEquals("b", ret);

        ret = eval("1 > 0 ? 'a'");
        assertEquals("a", ret);

        ret = eval("1 > 2 ? 'a'");
        assertNull(ret);

        ret = eval("1 ? 'a'");
        assertEquals("a", ret);

        ret = eval("null ? 'a'");
        assertNull(ret);
    }

    @Test
    public void testIfExpression() {
        Object ret = eval("'a' if 1 > 0");
        assertEquals("a", ret);

        ret = eval("'a' if 1 > 0 else 'b'");
        assertEquals("a", ret);

        ret = eval("'a' if 1 < 0 else 'b'");
        assertEquals("b", ret);

        ret = eval("'a' if 1 < 0");
        assertNull(ret);
    }

    @Test
    public void testAccessExpression() {
        String source = "user.name.toString + 'yyy'";
        Map<String, Object> env = new HashMap<>();
        User user = new User();
        user.setName("xxx");
        env.put("user", user);
        Object ret = eval(source, env);
        assertNotNull(ret);
        System.out.println(ret);
    }


    @Test
    public void testInvokeExpression() {
        String source = "'abcd'.charAt(1.5 * 2)";
        Object ret = eval(source);
        assertNotNull(ret);
        assertEquals(new Character('d'), ret);
        System.out.println(ret);
    }


    @Test
    public void testListIndex() {
        String source = "list[1]";
        Map<String, Object> env = new HashMap<>();
        env.put("list", Arrays.asList(2, 3, 5));
        Object ret = eval(source, env);
        assertEquals(new Integer(3), ret);
        assertNotNull(ret);
    }

    @Test
    public void testMapIndex() {
        String source = "map['b'] * 2";
        Map<String, Object> env = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        env.put("map", map);
        Object ret = eval(source, env);
        assertEquals(new Integer(4), ret);
        assertNotNull(ret);
    }


    @Test
    public void testArrayIndex() {
        String source = "arr[1] * 2";
        Map<String, Object> env = new HashMap<>();
        env.put("arr", new int[] {2, 4, 5});
        Object ret = eval(source, env);
        assertEquals(new Integer(8), ret);
        assertNotNull(ret);
    }

    @Test
    public void testNullIndex() {
        Throwable th = null;
        try {
            eval("[0, 1, 2][null]");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

    }

    @Test
    public void testErrorIndex() {
        Map<String, Object> env = new HashMap<>();
        env.put("a", new A());
        Throwable th = null;
        try {
            eval("a[0]");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

        th = null;
        try {
            eval("a['x']");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);

        th = null;
        try {
            eval("null[0]");
        } catch (Throwable e) {
            th = e;
        }
        assertNotNull(th);
    }

    @Test
    public void testCastReturnTypes() {
        EasyElContext context = new EasyElContext();

        Boolean bret = EasyEl.evalAsBoolean("true", context);
        assertNotNull(bret);
        assertTrue(bret);

        String strRet = EasyEl.evalAsString("123.0", context);
        assertNotNull(strRet);
        assertEquals("123.0", strRet);

        Integer iret = EasyEl.evalAsInteger("123.0", context);
        assertNotNull(iret);
        assertEquals(new Integer(123), iret);

        Short sret = EasyEl.evalAsShort("123.0", context);
        assertNotNull(iret);
        assertEquals(new Short("123"), sret);

        Long lret = EasyEl.evalAsLong("123.0", context);
        assertNotNull(iret);
        assertEquals(new Long(123l), lret);

        BigInteger biret = EasyEl.evalAsBigInteger("123.0", context);
        assertNotNull(biret);
        assertEquals(new BigInteger("123"), biret);

        Float fret = EasyEl.evalAsFloat("123.0", context);
        assertNotNull(iret);
        assertEquals(new Float(123.0f), fret);

        Double dret = EasyEl.evalAsDouble("123.0", context);
        assertNotNull(dret);
        assertEquals(new Double(123.0D), dret);

        BigDecimal bdret = EasyEl.evalAsBigDecimal("123.0", context);
        assertNotNull(bdret);
        assertEquals(new BigDecimal("123.0"), bdret);
    }

}
