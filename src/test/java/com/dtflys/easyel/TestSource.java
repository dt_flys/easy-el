package com.dtflys.easyel;

import com.dtflys.easyel.compile.EasyElSource;
import com.dtflys.easyel.compile.EasyElSourceLine;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestSource {
    
    @Test
    public void testReadSourceLine() {
        String text = "a + b";
        EasyElSource source = new EasyElSource(text, "script");
        EasyElSourceLine line = source.nextLine();
        assertThat(line).isNotNull();
        assertThat(line.getLineNumber()).isEqualTo(1);
    }


    @Test
    public void testReadSourceLine2() {
        String text = "a + b\nc + d\n2002332 - 1";
        EasyElSource source = new EasyElSource(text, "script");
        
        // first line
        EasyElSourceLine line = source.nextLine();
        assertThat(line).isNotNull();
        assertThat(line.getLineNumber()).isEqualTo(1);
        assertThat(line.getText()).isEqualTo("a + b");

        // second line
        line = source.nextLine();
        assertThat(line).isNotNull();
        assertThat(line.getLineNumber()).isEqualTo(2);
        assertThat(line.getText()).isEqualTo("c + d");
        
        // third line
        line = source.nextLine();
        assertThat(line).isNotNull();
        assertThat(line.getLineNumber()).isEqualTo(3);
        assertThat(line.getText()).isEqualTo("2002332 - 1");
    }


    @Test
    public void testReadSourceReadWindow() {
        String text = "";
        int lines = 3000;
        
        // build lines
        for (int i = 0; i < lines; i++) {
            text += "line xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" + i;
            if (i < lines - 1) {
                text += "\n";
            }
        }
        
        EasyElSource source = new EasyElSource(text, "script");
        for (int i = 0; i < lines; i++) {
            EasyElSourceLine line = source.nextLine();
            assertThat(line).isNotNull();
            assertThat(line.getLineNumber()).isEqualTo(i + 1);
            String lineText = "line xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" + i;
            if (i < lines - 1) {
                lineText += "\n";
            }
            assertThat(line.getText()).isEqualTo(lineText);
        }
        
        assertThat(source.getReadWindowLines().size()).isEqualTo(EasyElSource.READ_WINDOW_SIZE);
        assertThat(source.getReadWindowStartLine()).isEqualTo(lines - EasyElSource.READ_WINDOW_SIZE + 1);
        assertThat(source.getReadWindowEndLine()).isEqualTo(lines);
        
        assertThat(source.getReadWindowLines().get(0).getLineNumber()).isEqualTo(lines - EasyElSource.READ_WINDOW_SIZE + 1);
        assertThat(source.getReadWindowLines().get(source.getReadWindowLines().size() - 1).getLineNumber()).isEqualTo(lines);
    }


}
