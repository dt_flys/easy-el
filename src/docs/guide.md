## 1 简介

EASYEL一是种嵌入Java代码中执行的表达式语言，类似EL和OGNL。但在语言能力上更为强大。

### 1.1 架构

![architect](images/architect.png)

## 2 快速开始


### 2.1 Maven配置

```xml
    <!-- 暂未发布中央仓库, 敬请期待 -->
    <dependency>
        <groupId>com.dtflyx</groupId>
        <artifactId>easy-el</artifactId>
        <version>1.0.0</version>
    </dependency>
```


### 2.2 Hello World

```java
EasyElContext context = new EasyElContext();
Object ret = EasyEl.eval("'hello world!'", context);
System.out.println(ret);
```


## 3 关键字

关键字是EASYEL语言中语法的组成部分，变量名不能和关键字重合。
下列是EASYEL中定义的关键字：

```
    true    false    not    in    and    or    null
    
```


## 4 基本数据类型

### 4.1 类型总览

| 类型 | 表达式 | 描述 |
|:-----|:--------------------------|:--------------|
| 整形   | 1 , -100 , 490   | 32位、有符号的以二进制补码表示的整数 (范围: -2^31 ~ 2^31 - 1) |
| 长整形 | 34L , 9999999999 | 64位、有符号的以二进制补码表示的整数 (范围: -2^63 ~ 2^63 -1) |
| 大整形 | 99999999999999, -392392342899234234 | 无限位数有符号的整数 (范围: 负无穷到正无穷) |
| 单精度浮点数 | 3.45f, 5.3F    | 32位、有符号的单精度浮点数  | 
| 双精度浮点数 | 3.45d, 5.3D    | 64位、有符号的双精度浮点数  |
| 大数 | 3923923.43535423 ， 99999999999999.9999999   | 无限精度小数  |
| 字符串 | 'abc' , "abc"               | 用双引号或单引号括起 |
| 日期   | 2018-01-02 00:10:02 , 2017/05/01 , 2018\10\11       | 日期和时间      |
| 区间   | \[-30..30\] , \(4..100\] , \[3..7\)  | 表示范围，类似数学上的区间概念 |
| 列表   | \[1, 2, 4\] , \['a', 'b', 4, 5\]  | 同Java的List和数组 |
| Map   | {name: 'haha', age: 12} , {'a': 1, 'b': 2} | 键值映射表 |


### 4.2 EASYEL类型与Java类型的映射关系

| EASYEL类型 | Java类型 |
|:--------|:---------|
| 整形     | java.lang.Integer  |
| 长整形   | java.lang.Long     |
| 大整形   | java.math.BigInteger |
| 单精度浮点数 | java.lang.Float |
| 双精度浮点数 | java.lang.Double |
| 大数     | java.math.BigDecmial |
| 字符串 | java.lang.String |
| 日期 | EasyElDate |
| 区间 | EasyElRange |
| 列表 | java.util.ArrayList |
| Map | java.util.HashMap |

### 4.3 数字

EASYEL的数字包含了所有整数和浮点数，不必考试其数字是int类型、long类型还是float类型。


#### 4.3.1 表达式文法
```

<整数> ::=  0 | [1-9] [0-9]*

<数值> ::= <整数> '.' [0-9]+

<数字> ::= <整数> | <数值>

```

#### 4.3.2 Example

```
3
1.01
-3
3902999999999
9.9999999999
```

### 4.4 字符串

#### 4.4.1 表达式文法
```
<双引号字符>  ::=  ~["\\] | <转义字符列表>
<单引号字符>  ::=  ~['\\] | <转义字符列表>
<字符串>     ::= '"' <双引号字符>* '"' | '\'' <单引号字符>* '\''
```

#### 4.4.2 Example

```
'abcd'
'我想说: "很好!"'
"233333"
"Hello 'man'"
```

### 4.5 日期

#### 4.5.1 表达式文法
```
<年>      ::=  [0-9] [0-9] [0-9] [0-9]

<月日>    ::=  '-' [0-9] [0-9] '-' [0-9] [0-9]
            |  '.' [0-9] [0-9] '.' [0-9] [0-9]
            |  '\' [0-9] [0-9] '\' [0-9] [0-9]
            |  '/' [0-9] [0-9] '/' [0-9] [0-9]

<年月日>  ::=  <年> <月日>

<时分>    ::=  [0-9] [0-9] ':' [0-9] [0-9]

<时分秒>  ::=  <时分> ':' [0-9] [0-9]

<时分秒毫秒>
         ::=  <时分秒> ':' [0-9] [0-9]

<日期>    ::= <年月日> (<时分> | <时分秒> | <时分秒毫秒>)?

```

#### 4.5.2 Example

```
2018-01-02
2018/01/02
2018\01\02

2018-01-02 00:00
2018-01-02 00:00:00
2018-01-02 00:00:00:00
```


### 4.6 区间

#### 4.6.1 表达式文法

```
<区间> ::= '[' <表达式> '..' <表达式> ']'
         | '[' <表达式> '..' <表达式> ')'
         | '(' <表达式> '..' <表达式> ']'
         | '(' <表达式> '..' <表达式> ')'
 ```
 
 
#### 4.6.2 Example
 
 ```
[-100 .. 100] # 全开区间
(-100 .. 100) # 全闭区间
[-100 .. 100) # 前开后闭区间
(-100 .. 100] # 前闭后开区间
```


## 5 语法

### 5.1 操作符

优先级排序的值越小，优先级越高。在执行表达式时，优先级高的操作符会先执行，优先级低的后执行。

| 操作符      | 前置操作符 | 结合性 | 描述 | 优先级 |
|:-----------|:----------|:------|:-----|:------|
| ()         | 是        | 从左至右 | 括号运算  | 1  |
| []         | 是        | 从左至右 | 下标运算 | 1  |
| not        | 是        | 从右至左 | 取否  | 2  |
|  !         | 是        | 从右至左 | 取否  | 2  |
|  +(正号)   | 是        | 从右至左  | 取正数 | 3   |
|  -(负号)   | 是        | 从右至左  | 取负数 | 3   |
| **         | 否        | 从左至右 | 指数  | 4   |
| *          | 否        | 从左至右 | 乘    | 5   |
| /          | 否        | 从左至右 | 除    | 6   |
| +          | 否        | 从左至右 | 加    | 7   |
| -          | 否        | 从左至右 | 减    | 7   |
| &          | 否        | 从左至右 | 位运算与 | 8  |
| &#124;     | 否        | 从左至右 | 位运算或 | 8  |
| not in     | 否        | 从左至右 | 不在...内 | 9 |
| &lt;       | 否        | 从左至右 | 小于  | 10 |
| &gt;       | 否        | 从左至右 | 大于  | 10 |
| &lt;=      | 否        | 从左至右 | 小于等于 | 11 |
| &gt;=      | 否        | 从左至右 | 大于等于 | 11 |
| in         | 否        | 从左至右 | 在...内 | 11 |
| !in        | 否        | 从左至右 | 不在...内 | 11 |
| ==         | 否        | 从左至右 | 相等   | 11 |
| !=         | 否        | 从左至右 | 不相等 | 11 |
| and        | 否        | 从左至右 | 逻辑与 | 12 |
| &&         | 否        | 从左至右 | 逻辑与 | 12 |
| or          | 否       | 从左至右 | 逻辑或 | 12 |
| &#124;&#124;| 否       | 从左至右 | 逻辑或 | 12 |
| ? :    | 否        | 从右至左 | 三目条件运算 | 13 |
| ?      | 否        | 从右至左 | 条件运算(取真值) | 13 |
| ?:     | 否        | 从右至左 | 条件运算（非真即假） | 13 |


### 5.2 单目运算操作符

```
not true
```

结果：false

```
!(true)
```

结果: false

```
+(32)
```

结果：32

```
-(32)
```

结果：-32


### 5.3 算术运算符


`+` : 加法运算符 - 将左右两边的值相加

`-` : 减法运算符 - 将左右两边的值相减

`*` : 乘法运算符 - 将左右两边的值相乘

`/` : 除法运算符 - 计算左操作数除以右操作数

`**` : 指数运算符 - 以左操作数为底数，右操作符为指数，计算出指数幂结果

`%` : 取余运算符 - 计算左操作数除以右操作数的余数

四则运算

```
3 + 4      # 结果：7

3 + 4 * 2  # 结果：11

100 / (5 * 2) - 1  # 结果：9
```

指数运算

```
10 ** 2   # 结果：100

2 ** 3    # 结果：8
```

取余运算

```
5 % 2     # 结果：1

10 % 4    # 结果：2
```

### 5.4 关系运算符

`==` : 相等操作符 - 检查左右两边的值是否相等，如果相等则为真，反之为假。

`!=` : 不等于操作符 - 检查左右两边的值是否相等，如果不相等则为真，反之为假。

`>` : 大于操作符 - 检查左操作数是否大于右操作数，如果是则为真，反之为假。

`<` : 小于操作符 - 检查左操作数是否小于右操作数，如果是则为真，反之为假。

`>=` : 大于等于操作符 - 检查左操作数是否大于或等于右操作数，如果是则为真，反之为假。

`<=` : 小于等于操作符 - 检查左操作数是否小于或等于右操作数，如果是则为真，反之为假。


相等操作

```
1 == 1       # true
1 + 3 == 4   # true
1 == 3       # false

'abc' == 'abc'       # true
'ab' + 'c' == 'abc'  # true 
'1' == 1             # false
```

不等于操作

```
1 != 2    # true
1 != '1'  # true
1 != 1    # false

'abc' != 'aaa'       # true
'ab' + 'c' != 'abc'  # false
```

大于/小于/大于等于/小于等于操作

```
32 > 5     # true
32 > 32    # false
32 >= 32   # true
32 > 100   # false
32 < 100   # true
32 < 32    # false
32 <= 100  # true
```


### 5.5 存在运算符

`in` : 存在操作符 - 检查左边的值是否存在于右边的值之中，如果存在则为真，反之为假。

`not in`、`!in` : 不存在操作符 - 检查左边的值是否存在于右边的值之中，如果不存在则为真，反之为假。

IN操作

```
3 in [1, 3, 5, 7]     # true
20 in [0 .. 100]      # true
-100 in (-100 .. 100] # false
'ok' in 'I am ok'     # true

2018-03-04 in [2018-01-01 .. 2019-01-01) # true
```

NOT IN操作

```
0 not in [1, 2, 3]  # true
1 not in [0 .. 1)   # true
'a' !in ['A', 'B']  # true

'b' !in 'hello'     # true
'ok' not in 'It is ok'  # false
```

### 5.6 正则运算符

`=~` : 正则匹配操作符 - 检查左边的字符串是否匹配右边的正则表达式，如果是则为真，反之为假。

`!=~` : 正则不匹配操作符 - 检查左边的字符串是否匹配右边的正则表达式，如果不匹配则为真，反之为假。

正则匹配操作

```
'abc' =~ '[a-z]+'         # true
'haha123' =~ 'haha[0-9]+' # true
```

正则不匹配操作

```
'abc' !=~ '[a-z]+'        # false
'haha123' !=~ '[\\d]+'    # false
```

### 5.7 变量



### 5.8 方法调用

### 5.9 创建对象



